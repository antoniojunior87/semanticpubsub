package br.uniriotec.fwbroker.util;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.IAttribute;

@Service("similaridadeKeyGenerator")
public class SimilaridadeKeyGenerator implements KeyGenerator {

	@Override
	public Object generate(Object target, Method method, Object... params) {

		IAttribute subscriptionAttribute = (IAttribute) params[0];

		IAttribute notificationAttribute = (IAttribute) params[1];

		String key = String.format("{%s=%s}", subscriptionAttribute.getName(), notificationAttribute.getName());

		return key;
	}

}
