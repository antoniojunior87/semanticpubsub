package br.uniriotec.fwbroker.util;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.fwbroker.entity.Attribute;
import br.uniriotec.fwbroker.entity.Notification;
import br.uniriotec.fwbroker.entity.Value;
import br.uniriotec.fwbroker.entity.Word;
import br.uniriotec.fwbroker.semantic.nlp.NLPService;

@Service("hasanNotificationConverter")
public class HasanNotificationConverter {

	@Autowired
	private NLPService nlpService;

	/*
	 * 136-2 {measurement unit=kilometre per hour, continent=european countries,
	 * type=artificial precipitation speed slight decrease event, residential
	 * area=lubeck, country=west germany}
	 */

	public Notification newNotification(String evento) {

		evento = evento.replaceAll("~esa", "");

		String[] partes = evento.split("\\{");
		String id = partes[0].trim();
		String atributos = partes[1].replaceAll("\\{", "").replaceAll("\\}", "");

		Notification newNotification = new Notification();
		newNotification.setId(id);
		newNotification.setOriginalEvent(evento);
		newNotification.setAttributes(convert(atributos));
		return newNotification;
	}

	private Attribute[] convert(String atributos) {

		String[] predicados = atributos.split(", ");

		ArrayList<Attribute> attributes = new ArrayList<>();

		for (String predicado : predicados) {
			attributes.add(converterPredicado(predicado));
		}

		return attributes.toArray(new Attribute[0]);
	}

	private Attribute converterPredicado(String predicado) {

		String[] partes = predicado.split("=");
		String nome = partes[0].trim();
		String valor = partes[1].trim();

		Word[] words = nlpService.getWords(nome);

		return new Attribute(nome, words, new Value(valor));
	}

}
