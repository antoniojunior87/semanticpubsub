package br.uniriotec.fwbroker.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import br.uniriotec.fwbroker.entity.Attribute;
import br.uniriotec.fwbroker.entity.Notification;
import br.uniriotec.fwbroker.entity.Value;
import br.uniriotec.fwbroker.semantic.nlp.NLPService;

@SuppressWarnings("rawtypes")
@Service("jsonNotificationConverter")
public class JsonNotificationConverter {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private NLPService nlpService;

	public Notification newNotification(String json, boolean concatParentName) throws IOException {

		String[] partes = json.split("\t");

		String id = partes[0];
		String sJson = partes[1];

		Notification newNotification = new Notification();
		// newNotification.setId(UUID.randomUUID().toString());
		newNotification.setId(id);
		newNotification.setOriginalEvent(sJson);
		newNotification.setAttributes(convert(sJson, concatParentName));
		return newNotification;
	}

	private Attribute[] convert(String json, boolean concatParentName) throws JsonProcessingException, IOException {

		JsonNode root = om.readTree(json);

		// json sempre comeca com um object ou um array
		// exemplo: {} ou [1,2,{}]
		ArrayList<Attribute> attributes = converterContainerNode((ContainerNode) root, "", concatParentName);

		return attributes.toArray(new Attribute[0]);
	}

	private ArrayList<Attribute> converterContainerNode(ContainerNode parent, String parentName,
			boolean concatParentName) {

		Iterator<String> fieldsNames = parent.fieldNames();

		ArrayList<Attribute> attributes = new ArrayList<>();

		// node tem filhos sem nome
		// percorre os filhos dos filhos
		if (parent.size() > 0 && !fieldsNames.hasNext()) {
			Iterator<JsonNode> iterator = parent.elements();
			while (iterator.hasNext()) {
				JsonNode child = iterator.next();

				if (child.isContainerNode()) {
					// arrays e objects sao adicionados como Attributes sem
					// valor
					attributes.addAll(converterContainerNode((ContainerNode) child, parentName, concatParentName));
				}
			}
		} else {

			// so devem ser considerados os campos que contem nome
			// porque os nomes serao usados para avaliacao da similaridade
			while (fieldsNames.hasNext()) {
				String fieldName = fieldsNames.next();
				JsonNode child = parent.get(fieldName);

				String nomeNormalizado = prepararNome(fieldName);

				String nome = concatParentName ? (parentName + " " + nomeNormalizado) : nomeNormalizado;

				if (child.isContainerNode()) {
					// arrays e objects sao adicionados como Attributes sem
					// valor
					attributes.add(new Attribute(nome, nlpService.getWords(nome)));
					// e os filhos sao adicionados como novos Attributes
					attributes.addAll(converterContainerNode((ContainerNode) child, nomeNormalizado, concatParentName));
				} else if (child.isValueNode()) {
					attributes
							.add(new Attribute(nome, nlpService.getWords(nome), converterValueNode((ValueNode) child)));
				}
			}
		}

		return attributes;
	}

	private Value converterValueNode(ValueNode node) {

		switch (node.getNodeType()) {
		case BOOLEAN:
			return new Value(node.booleanValue());
		case NUMBER:
			NumericNode numeric = (NumericNode) node;

			switch (numeric.numberType()) {
			case BIG_DECIMAL:
			case DOUBLE:
			case FLOAT:
				return new Value(numeric.doubleValue());
			case BIG_INTEGER:
			case INT:
			case LONG:
				return new Value(numeric.longValue());
			default:
				return null;
			}
		case STRING:
			return new Value(node.textValue());
		default:
			return null;
		}

	}

	/**
	 * Substitui qualquer caracter diferente de letras, numeros, espaço em
	 * branco e hifen por espaço em branco. Alem disso separa palavras escritas
	 * em CamelCase.
	 * 
	 * @param s
	 * @return
	 */
	private String prepararNome(String s) {
		return splitCamelCase(s).replaceAll("[^\\- A-Za-z0-9]", "");
	}

	private String splitCamelCase(String s) {
		return s.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
				"(?<=[A-Za-z])(?=[^A-Za-z])"), " ");
	}

}
