package br.uniriotec.fwbroker.util;

public interface LogPerformance {

	public static final String NOME_LOG = "br.uniriotec.performance";

	public static final String TOKEN_EVENTO_RECEBIDO = "{},tag_evento_recebido,{}";
	public static final String TOKEN_EVENTO_PROCESSADO = "{},tag_evento_processado,{}";
	public static final String TOKEN_EVENTO_ENVIADO_SUBS = "{},tag_evento_enviado,{},{}";
	public static final String TOKEN_NOVA_SUBSCRICAO = "{},tag_nova_subscricao,{}";

}
