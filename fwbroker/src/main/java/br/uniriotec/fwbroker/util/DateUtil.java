package br.uniriotec.fwbroker.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;

public class DateUtil {

	/**
	 * Não mudar a ordem dos formatos.
	 */
	private static final String[] PATTERNS = { 
			"yyyy-MM-dd'T'HH:mm:ss'Z'",
			"yyyy-MM-dd HH:mm:ss",
			"yyyy-MM-dd'T'HH:mm:ssZZ",
			"yyyy-MM-dd HH:mm:ssZZ",
			"yyyy-MM-dd",
			"MM-dd-yyyy",
			"yyyy/MM/dd'T'HH:mm:ss",
			"yyyy/MM/dd'T'HH:mm:ssZZ",
			"yyyy/MM/dd HH:mm:ss",
			"yyyy/MM/dd HH:mm:ssZZ",
			"yyyy/MM/dd",
			"dd-MM-yyyy",
			"dd/MM/yyyy",
			"dd/MM/yyyy'T'HH:mm:ss",
			"dd/MM/yyyy HH:mm:ss",
			"dd/MM/yyyy'T'HH:mm:ssZZ",
			"dd/MM/yyyy HH:mm:ssZZ",
	};

	private static final FastDateFormat FDF = FastDateFormat.getInstance(PATTERNS[0]);

	public static boolean canBeDate(String string) {
		return getDate(string) != null;
	}

	public static Date getDate(String string) {
		try {
			return DateUtils.parseDateStrictly(string, PATTERNS);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String toFormatedString(Date date) {
		return FDF.format(date);
	}
}
