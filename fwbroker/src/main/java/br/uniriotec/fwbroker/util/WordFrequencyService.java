package br.uniriotec.fwbroker.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * Corpus extraido do Dump das paginas Wikipedia.
 * <p>
 * Projeto https://github.com/IlyaSemenov/wikipedia-word-frequency
 * <p>
 * O projeto usou a ferramenta wikiextractor:
 * https://github.com/attardi/wikiextractor
 * <p>
 * O projeto foi inspirado nesse artigo:
 * http://imonad.com/seo/wikipedia-word-frequency-list/
 * <p>
 * Data da extração: 2015-05-02.
 * <p>
 * Total different words: 1.901.124
 * <p>
 * Total word uses: 1.562.759.958
 * <p>
 * Top 20 most popular words: the, of, and, in, to, was, is, for, as, on, with,
 * by, he, that, at, from, his, it, an, were.
 */
@Component("wordFrequencyService")
public class WordFrequencyService {

	private static final Logger LOG = LoggerFactory.getLogger(WordFrequencyService.class.getName());

	private static final String OUTPUT_FOLDER = "/tmp";
	private static final String FILE = OUTPUT_FOLDER + File.separator + "words-frequency.txt";

	@Value(value = "classpath:words-frequency.zip")
	private Resource zipFile;

	private Map<String, Integer> wordsFrequency;

	public Integer getFrequency(String word) {
		try {
			return getWordsFrequency().get(word.toLowerCase());
		} catch (Exception e) {
		}
		return 0;
	}

	public Integer getCorpusSize() {
		try {
			return getWordsFrequency().size();
		} catch (Exception e) {
			LOG.error("Error: failed to get word frequency", e);
		}
		return null;
	}

	private Map<String, Integer> getWordsFrequency() throws Exception {

		if (wordsFrequency != null) {
			return wordsFrequency;
		}

		wordsFrequency = new HashMap<>();

		try (BufferedReader br = new BufferedReader(new FileReader(getUnzipedFile()))) {

			String line;

			while ((line = br.readLine()) != null) {

				String[] lineContent = line.split(" ");

				String word = lineContent[0];
				String frequency = lineContent[1];

				wordsFrequency.put(word.toLowerCase(), Integer.valueOf(frequency));
			}
		}

		return wordsFrequency;
	}

	private File getUnzipedFile() {

		File wordsFrequencyFile = new File(FILE);

		if (wordsFrequencyFile.exists()) {
			return wordsFrequencyFile;
		}

		byte[] buffer = new byte[1024];

		try {

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(zipFile.getInputStream());
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(OUTPUT_FOLDER + File.separator + fileName);

				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return new File(FILE);
	}

	public static void main(String[] args) {

		System.out.println(new WordFrequencyService().getFrequency("european"));
	}
}
