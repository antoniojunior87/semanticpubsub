package br.uniriotec.fwbroker.util;

import java.util.ArrayList;

import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.Operator;
import br.uniriotec.fwbroker.entity.Constraint;
import br.uniriotec.fwbroker.entity.Subscription;
import br.uniriotec.fwbroker.entity.Value;
import br.uniriotec.fwbroker.semantic.nlp.NLPService;

/**
 * at1, op1, vl1; at2, op2, vl3
 */
@Service("subscriptionConverter")
public class SubscriptionConverter {

	@Autowired
	private NLPService nlpService;

	public Subscription newSubscription(String subscription, String callback) {

		String[] partes = subscription.split("\t");

		String id = partes[0];
		String sSubscription = partes[1];

		Subscription newSubscription = new Subscription();
		// newSubscription.setId(UUID.randomUUID().toString());
		newSubscription.setId(id);
		newSubscription.setCallback(callback);
		newSubscription.setOriginalSubscription(sSubscription);
		newSubscription.setConstraints(convert(sSubscription));
		return newSubscription;
	}

	private Constraint[] convert(String subscription) {

		String[] sConstraints = subscription.split(";");

		ArrayList<Constraint> constraints = new ArrayList<>();
		for (String sConstraint : sConstraints) {
			String[] s = sConstraint.split(",");
			Constraint b = new Constraint();
			b.setName(s[0]);
			b.setWords(nlpService.getWords(s[0]));

			if (s.length > 1) {
				b.setOperator(Operator.valueOf(s[1].trim()));
			}

			if (s.length > 2) {
				b.setValue(convertBrokerValue(s[2]));
			}

			constraints.add(b);
		}

		return constraints.toArray(new Constraint[0]);
	}

	private Value convertBrokerValue(String value) {

		if (StringUtils.isNumber(value)) {
			return new Value(Double.valueOf(value));
		} else if ("true".equals(value.toLowerCase()) || "false".equals(value.toLowerCase())) {
			return new Value(Boolean.valueOf(value));
		} else {
			return new Value(value);
		}

	}

}
