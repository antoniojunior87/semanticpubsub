package br.uniriotec.fwbroker.util;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.Operator;
import br.uniriotec.fwbroker.entity.Constraint;
import br.uniriotec.fwbroker.entity.Subscription;
import br.uniriotec.fwbroker.entity.Value;
import br.uniriotec.fwbroker.entity.Word;
import br.uniriotec.fwbroker.semantic.nlp.NLPService;

/**
 * 13-1 {category~esa=energy consumption sharp increase event~esa,room~esa=gound
 * floor kitchen~esa,wing~esa=ground floor north wing~esa,floor~esa=ground
 * floor~esa,country~esa=ireland~esa}
 */
@Service("hasanSubscriptionConverter")
public class HasanSubscriptionConverter {

	private static final String SIMILAR = "~esa";

	@Autowired
	private NLPService nlpService;

	public Subscription newSubscription(String subscription, String callback) {

		String[] partes = subscription.split("\\{");
		String id = partes[0].trim();
		String constraints = partes[1].replaceAll("\\{", "").replaceAll("\\}", "");

		Subscription newSubscription = new Subscription();
		newSubscription.setId(id);
		newSubscription.setCallback(callback);
		newSubscription.setOriginalSubscription(subscription);
		newSubscription.setConstraints(convert(constraints));
		return newSubscription;
	}

	private Constraint[] convert(String sConstraints) {

		String[] predicados = sConstraints.split(",");

		ArrayList<Constraint> constraints = new ArrayList<>();

		for (String predicado : predicados) {
			constraints.add(converterPredicado(predicado));
		}
		return constraints.toArray(new Constraint[0]);
	}

	private Constraint converterPredicado(String predicado) {

		String[] partes = predicado.split("=");
		String nome = partes[0].replaceAll(SIMILAR, "").trim();
		String valor = partes[1].trim();

		Word[] words = nlpService.getWords(nome);

		Constraint constraint = new Constraint();
		constraint.setName(nome);

		Operator op = Operator.EQ;

		if (valor.contains(SIMILAR)) {
			op = Operator.SM;
		}

		constraint.setOperator(op);
		constraint.setWords(words);
		constraint.setValue(new Value(valor.replaceAll(SIMILAR, "")));

		return constraint;
	}

}
