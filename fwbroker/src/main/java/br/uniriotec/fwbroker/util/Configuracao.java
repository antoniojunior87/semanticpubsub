package br.uniriotec.fwbroker.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("fwbroker")
public class Configuracao {

	private String tipoEvento;

	private String metodoSimilaridade;

	private String similaridadeSentencas;

	private Double threshold;

	private boolean concatenarNomeNodeParent;

	private String tipoChecagem;

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getMetodoSimilaridade() {
		return metodoSimilaridade;
	}

	public void setMetodoSimilaridade(String metodoSimilaridade) {
		this.metodoSimilaridade = metodoSimilaridade;
	}

	public String getSimilaridadeSentencas() {
		return similaridadeSentencas;
	}

	public void setSimilaridadeSentencas(String similaridadeSentencas) {
		this.similaridadeSentencas = similaridadeSentencas;
	}

	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	public boolean isConcatenarNomeNodeParent() {
		return concatenarNomeNodeParent;
	}

	public void setConcatenarNomeNodeParent(boolean concatenarNomeNodeParent) {
		this.concatenarNomeNodeParent = concatenarNomeNodeParent;
	}

	public String getTipoChecagem() {
		return tipoChecagem;
	}

	public void setTipoChecagem(String tipoChecagem) {
		this.tipoChecagem = tipoChecagem;
	}

}