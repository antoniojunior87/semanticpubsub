package br.uniriotec.fwbroker.semantic.similaridade;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.IWord;
import br.uniriotec.broker.framework.core.ISentenceSimilarity;
import br.uniriotec.fwbroker.util.Configuracao;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

@Service("averageSentenceSimilarity")
public class AverageSentenceSimilarity implements ISentenceSimilarity {

	private static final Logger LOG = LoggerFactory.getLogger(AverageSentenceSimilarity.class);

	protected static final int ESCALA = 3;

	protected static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

	private RelatednessCalculator metodoSimilaridade;
	private NictWordNet db;

	@Autowired
	public AverageSentenceSimilarity(Configuracao configuracao) {

		WS4JConfiguration.getInstance().setCache(true);
		db = new NictWordNet();

		// wup, lin, resnik, jiang
		switch (configuracao.getMetodoSimilaridade()) {
		case "wup":
			metodoSimilaridade = new WuPalmer(db);
			break;
		case "lin":
			metodoSimilaridade = new Lin(db);
			break;
		case "resnik":
			metodoSimilaridade = new Resnik(db);
			break;
		case "jiang":
			metodoSimilaridade = new JiangConrath(db);
			break;
		default:
			LOG.error("invalid parameter 'fwbroker.metodo-similaridade'");
			throw new InvalidParameterException("invalid parameter 'fwbroker.metodo-similaridade'");
		}
	}

	public double computeSimilarity(IWord[] subscriptionWords, IWord[] notificationWords) {
		return calcularSimilaridade(subscriptionWords, notificationWords);
	}

	private double calcularSimilaridade(IWord[] subscriptionWords, IWord[] notificationWords) {

		try {
			BigDecimal similaridadeCalculada = this.obterSimilaridade(subscriptionWords, notificationWords);

			return similaridadeCalculada.setScale(ESCALA, ROUNDING_MODE).doubleValue();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0.0d;
	}

	private BigDecimal obterSimilaridade(IWord word1, IWord word2) {

		double dSimilaridade = metodoSimilaridade.calcRelatednessOfWords(word1.getLemma(), word2.getLemma());

		dSimilaridade = dSimilaridade >= 1d ? 1d : dSimilaridade;

		BigDecimal bSimilaridade = new BigDecimal(dSimilaridade);

		return bSimilaridade.setScale(ESCALA, ROUNDING_MODE);
	}

	private BigDecimal obterSimilaridade_old(IWord[] listPalavraX, IWord[] listPalavraY) {

		if (listPalavraX.length == 0 || listPalavraY.length == 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal somatorioX = BigDecimal.ZERO;
		BigDecimal somatorioY = BigDecimal.ZERO;

		int countSinonimosX = 0;
		for (IWord pX : listPalavraX) {

			countSinonimosX++;

			BigDecimal max = BigDecimal.ZERO;

			for (IWord pY : listPalavraY) {

				// obtem a similaridade
				BigDecimal similaridade = obterSimilaridade(pX, pY);

				if (similaridade.compareTo(max) > 0) {
					max = similaridade;
				}
			}

			somatorioX = somatorioX.add(max);

		}

		int countSinonimosY = 0;
		for (IWord pY : listPalavraY) {

			countSinonimosY++;

			BigDecimal max = BigDecimal.ZERO;

			for (IWord pX : listPalavraX) {

				// obtem a similaridade
				BigDecimal similaridade = obterSimilaridade(pY, pX);

				if (similaridade.compareTo(max) > 0) {
					max = similaridade;
				}
			}

			somatorioY = somatorioY.add(max);

		}

		BigDecimal somatorio = somatorioX.add(somatorioY);

		// Integer tamanho = (listPalavraX.size() + listPalavraY.size());
		// FIXME - Como usar os contadores? O sinonimo tem o mesmo peso do
		// original na média?
		Integer tamanho = countSinonimosX + countSinonimosY;

		// Media
		if (!BigDecimal.ZERO.equals(somatorio)) {
			return somatorio.divide(new BigDecimal(tamanho), ROUNDING_MODE);
		} else {
			return BigDecimal.ZERO;
		}
	}

	private BigDecimal obterSimilaridade(IWord[] listPalavraX, IWord[] listPalavraY) {

		if (listPalavraX.length == 0 || listPalavraY.length == 0) {
			return BigDecimal.ZERO;
		}

		List<String> listLemaDestinoMapeado = new ArrayList<String>();

		BigDecimal similaridadeTotal = BigDecimal.ZERO;
		int numeroPares = 0;

		try {
			for (IWord pOrigem : listPalavraX) {
				boolean formouParMorfologico = false;

				for (IWord pDestino : listPalavraY) {

					// Nome
					if (pOrigem.getPos().equals("n") && pDestino.getPos().equals("n")) {
						formouParMorfologico = true;
					} // Verbo
					else if (pOrigem.getPos().equals("v") && pDestino.getPos().equals("v")) {
						formouParMorfologico = true;
					} // Adjetivo
					else if (pOrigem.getPos().equals("j") && pDestino.getPos().equals("j")) {
						formouParMorfologico = true;
					} // Adverbio
					else if (pOrigem.getPos().equals("r") && pDestino.getPos().equals("r")) {
						formouParMorfologico = true;
					}
					if (formouParMorfologico) {
						similaridadeTotal = similaridadeTotal.add(obterSimilaridade(pOrigem, pDestino));
						listLemaDestinoMapeado.add(pDestino.getLemma());
						numeroPares++;
					}
				}

				if (!formouParMorfologico) {
					if (pOrigem.getPos().equals("n") || pOrigem.getPos().equals("v") || pOrigem.getPos().equals("j")
							|| pOrigem.getPos().equals("r")) {
						numeroPares++;
					}
				}
			}

			for (IWord pDestino : listPalavraY) {

				if (pDestino.getPos().equals("n") || pDestino.getPos().equals("v") || pDestino.getPos().equals("j")
						|| pDestino.getPos().equals("r")) {

					boolean formouParMorfologico = false;

					for (String lemaDestinoMapeado : listLemaDestinoMapeado) {
						if (pDestino.getLemma().equals(lemaDestinoMapeado)) {
							formouParMorfologico = true;
							break;
						}
					}

					if (!formouParMorfologico) {
						numeroPares++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Media
		if (!BigDecimal.ZERO.equals(similaridadeTotal)) {
			return similaridadeTotal.divide(new BigDecimal(numeroPares), ROUNDING_MODE);
		} else {
			return similaridadeTotal;
		}
	}

}
