package br.uniriotec.fwbroker.semantic.similaridade;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.IWord;
import br.uniriotec.broker.framework.core.ISentenceSimilarity;
import br.uniriotec.fwbroker.util.Configuracao;
import br.uniriotec.fwbroker.util.WordFrequencyService;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

@Service("fengSentenceSimilarity")
public class FengSentenceSimilarity implements ISentenceSimilarity {

	private static final Logger LOG = LoggerFactory.getLogger(FengSentenceSimilarity.class);

	@Autowired
	private WordFrequencyService wordFrequencyService;

	private RelatednessCalculator metodoSimilaridade;

	private NictWordNet db;

	private Integer corpusSize;

	@Autowired
	public FengSentenceSimilarity(Configuracao configuracao) {
		WS4JConfiguration.getInstance().setCache(true);
		db = new NictWordNet();

		// wup, lin, resnik, jiang
		switch (configuracao.getMetodoSimilaridade()) {
		case "wup":
			metodoSimilaridade = new WuPalmer(db);
			break;
		case "lin":
			metodoSimilaridade = new Lin(db);
			break;
		case "resnik":
			metodoSimilaridade = new Resnik(db);
			break;
		case "jiang":
			metodoSimilaridade = new JiangConrath(db);
			break;
		default:
			LOG.error("invalid parameter 'fwbroker.metodo-similaridade'");
			throw new InvalidParameterException("invalid parameter 'fwbroker.metodo-similaridade'");
		}
	}

	@Override
	public double computeSimilarity(IWord[] subscriptionWords, IWord[] notificationWords) {

		double sentenceSimilarity = sentenceSimilarity(subscriptionWords, notificationWords);

		return sentenceSimilarity;
	}

	private double computeSimilarity(IWord word1, IWord word2) {

		double dSimilaridade = metodoSimilaridade.calcRelatednessOfWords(word1.getLemma(), word2.getLemma());

		dSimilaridade = dSimilaridade >= 1d ? 1d : dSimilaridade;

		return dSimilaridade;
	}

	/**
	 * -----------------------------------------------------------------
	 * --------------------- Sentence Similarity -----------------------
	 * -----------------------------------------------------------------
	 */

	/**
	 * S(s1|s2): Similarity between two s1 and s2.
	 * <p>
	 * 
	 * S(s1|s2) = λ*directRelevance(s1|s2) + [(1-λ) * indirectRelevance(s1|s2)]
	 * 
	 * <p>
	 * S = Where λ ∈ [0, 1] decides the relative contribution of direct and
	 * indirect relevance to the sentence similarity.
	 * <p>
	 * Feng set λ to 0.85.
	 */
	private double sentenceSimilarity(IWord[] enhancedS1, IWord[] enhancedS2) {

		double lambida = 0.51;

		double directRelevance = directRelevance(enhancedS1, enhancedS2);

		double indirectRelevance = indirectRelevance(enhancedS1, enhancedS2);

		double similarity = (lambida * directRelevance) + ((1d - lambida) * indirectRelevance);

		LOG.trace("FENG [dirRel={}][indirRel={}][sim={}][{}][{}]", directRelevance, indirectRelevance, similarity,
				enhancedS1, enhancedS2);

		return similarity;

	}

	/**
	 * -----------------------------------------------------------------
	 * --------------------- Indirect Relevance ------------------------
	 * -----------------------------------------------------------------
	 */

	/**
	 * <p>
	 * The function F is:
	 * <p>
	 * Fij = THE MAX OF 1 2 3:
	 * <p>
	 * 1) Fi-1 j-1 + s(xi,yj)
	 * <p>
	 * 2) Fi-1 j - d
	 * <p>
	 * 3) Fi j-1 - d
	 * <p>
	 * Where d is de penalty for the gap.
	 * <p>
	 * d = -1 * (SUM(s(w1,w2) / totalwordpairs))
	 * 
	 * @param enhancedS1
	 * @param enhancedS2
	 * @return
	 */
	private double indirectRelevance(IWord[] enhancedS1, IWord[] enhancedS2) {

		List<IWord> xArray = new ArrayList<>();
		List<IWord> yArray = new ArrayList<>();

		// get original words
		for (IWord w : enhancedS1) {
			if (!w.getIsSynonym()) {
				xArray.add(w);
			}
		}
		for (IWord w : enhancedS2) {
			if (!w.getIsSynonym()) {
				yArray.add(w);
			}
		}
		IWord[] x = xArray.toArray(new IWord[0]);
		IWord[] y = yArray.toArray(new IWord[0]);

		int m = x.length;
		int n = y.length;

		// add one more line and collumn
		double[][] f = new double[m + 1][n + 1];

		// starts line 0 and collumn 0 with 0
		for (int i = 0; i <= m; i++) {
			f[i][0] = 0d;
		}
		for (int j = 0; j <= n; j++) {
			f[0][j] = 0d;
		}

		double d = penalty(x, y);

		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {

				double value1 = f[i - 1][j - 1] + wordsSimilarityForIndirectRelevance(x[i - 1], y[j - 1]);

				double value2 = f[i - 1][j] - d;

				double value3 = f[i][j - 1] - d;

				f[i][j] = max(value1, value2, value3);

			}
		}

		// normalization, values between 0 and 1
		double indirectRelevance = f[m][n] / Math.max(m, n);
		return indirectRelevance > 0d ? (indirectRelevance < 1d ? indirectRelevance : 1d) : 0d;
	}

	/**
	 * Average semantic similarity score between all sentence pairs.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private double penalty(IWord[] x, IWord[] y) {

		double totalwordpairnumber = x.length * y.length;

		double totalSimilarity = 0d;

		for (int i = 0; i < x.length; i++) {

			IWord wi = x[i];

			for (int k = 0; k < y.length; k++) {

				IWord qk = y[k];

				// compute similarity
				double similarity = computeSimilarity(wi, qk);

				totalSimilarity += similarity;
			}
		}

		return totalSimilarity / totalwordpairnumber;
	}

	private double max(double value1, double value2, double value3) {

		if (value1 > value2) {
			return value1 > value3 ? value1 : value3;
		} else {
			return value2 > value3 ? value2 : value3;
		}
	}

	/**
	 * Be s(xi, yj) the similarity between xi and yj
	 * <p>
	 * s(xi, yj) = θ * s(xi, yj) IF s(xi, yj) >= ζ
	 * <p>
	 * OR s(xi, yj) = -1 IF s(xi, yj) < ζ
	 * <p>
	 * Where θ = 1.2 IF xi AND yj ARE BOTH NN or VB
	 * <p>
	 * Where θ = 1.0 IN OTHER CASES
	 * <p>
	 * AND ζ = 0.2 (the threshold)
	 * 
	 * @param xi
	 * @param yj
	 * @return
	 */
	private double wordsSimilarityForIndirectRelevance(IWord xi, IWord yj) {

		double thresold = 0.2;
		double omega = 1.0;

		String posXi = xi.getPos();
		String posYj = yj.getPos();

		if (("n".equals(posXi) && "n".equals(posYj)) || ("v".equals(posXi) && "v".equals(posYj))) {
			omega = 1.2;
		}

		double similarityComputed = computeSimilarity(xi, yj);

		if (similarityComputed >= thresold) {
			return omega * similarityComputed;
		} else {
			return -1d;
		}
	}

	/**
	 * -----------------------------------------------------------------
	 * ----------------------- Direct Relevance ------------------------
	 * -----------------------------------------------------------------
	 */

	/**
	 * 
	 * @param enhancedS1
	 * @param enhancedS2
	 * @return
	 */
	private double directRelevance(IWord[] enhancedS1, IWord[] enhancedS2) {

		// step1 s'1 x s'2
		double[] step1S1 = fengStep1(enhancedS1, enhancedS2);

		// step1 s'2 x s'1
		double[] step1S2 = fengStep1(enhancedS2, enhancedS1);

		// step2 s'1
		double[] step2S1 = fengStep2(enhancedS1, step1S1);

		// step2 s'2
		double[] step2S2 = fengStep2(enhancedS2, step1S2);

		double sigS1 = fengStep3(step2S1);

		double sigS2 = fengStep3(step2S2);

		double sizeS1 = enhancedS1.length;

		double sizeS2 = enhancedS2.length;

		double directRelevance = ((sizeS1 / (sizeS1 + sizeS2)) * sigS1) + ((sizeS2 / (sizeS2 + sizeS1)) * sigS2);

		return directRelevance;
	}

	/**
	 * Sig: relative significance on sentence.
	 * <p>
	 * For each word in s'1 compute its relative significance on sentence s2.
	 * <p>
	 * n is the minimal return of sig. Feng set it 0.05.
	 * <p>
	 * i.e: if sig < n then sig = n.
	 * 
	 * @param enhancedS1
	 * @param enhancedS2
	 * @return
	 */
	private double[] fengStep1(IWord[] enhancedS1, IWord[] enhancedS2) {

		// sig s'1 x s'2
		double[] sig = new double[enhancedS1.length];

		// for each word in s'1
		for (int i = 0; i < enhancedS1.length; i++) {

			IWord wi = enhancedS1[i];

			// num of s'2 words with the same POS
			double[] wordpairnumber = { 0d, 0d, 0d, 0d };

			double sum = 0;

			String wiPos = wi.getPos();
			int posType = 0;

			if (wiPos.equals("n")) {
				posType = 0;
			} else if (wiPos.equals("v")) {
				posType = 1;
			} else if (wiPos.equals("a")) {
				posType = 2;
			} else if (wiPos.equals("r")) {
				posType = 3;
			}

			for (int k = 0; k < enhancedS2.length; k++) {

				IWord qk = enhancedS2[k];

				// have the same Part-Of-Speech (POS) type
				// and take only s2 words (not enhanced)
				if (wiPos.equals(qk.getPos()) && !qk.getIsSynonym()) {

					// compute similarity
					double similarity = computeSimilarity(wi, qk);
					sum += similarity;

					// increment count of POS
					wordpairnumber[posType]++;
				}

			}

			// starts with the value of n.
			sig[i] = 0d;
			if (wordpairnumber[posType] != 0) {
				sig[i] = sum / wordpairnumber[posType];
			}
		}

		return sig;
	}

	/**
	 * Compute the relative significance of NN'1 , VB'1 ,ADJ'1 and ADV'1.
	 * 
	 * = SUM(1/|NN'1|) * sig(wi|s2) * ic(wi)
	 * 
	 * @param enhancedS
	 * @param sig
	 */
	private double[] fengStep2(IWord[] enhancedS, double[] sig) {

		double[] size = getSizeByType(enhancedS);

		double[] sum = { 0d, 0d, 0d, 0d };

		sum[0] = 0; // NN'1
		sum[1] = 0; // VB'1
		sum[2] = 0; // ADJ'1
		sum[3] = 0; // ADV'1

		for (int i = 0; i < enhancedS.length; i++) {

			IWord wi = enhancedS[i];

			int type = 0;

			if (wi.getPos().equals("n")) {
				type = 0;
			} else if (wi.getPos().equals("v")) {
				type = 1;
			} else if (wi.getPos().equals("a")) {
				type = 2;
			} else if (wi.getPos().equals("r")) {
				type = 3;
			}

			sum[type] += (1d / size[type]) * sig[i] * informationContent(wi);
		}

		return sum;
	}

	private double[] getSizeByType(IWord[] enhancedS) {

		double[] size = { 0d, 0d, 0d, 0d };

		for (int i = 0; i < enhancedS.length; i++) {

			IWord wi = enhancedS[i];

			if (wi.getPos().equals("n")) {
				size[0]++;
			} else if (wi.getPos().equals("v")) {
				size[1]++;
			} else if (wi.getPos().equals("a")) {
				size[2]++;
			} else if (wi.getPos().equals("r")) {
				size[3]++;
			}
		}

		return size;
	}

	/**
	 * IC: information content.
	 * <p>
	 * IC = 1 - (log(n) / log(N))
	 * <p>
	 * n is the frequency of word w
	 * <p>
	 * N is the total number of words in the corpus
	 * 
	 * @param w
	 */
	private double informationContent(IWord wi) {

		// obter frequencia da palavra no corpus: n
		Integer iWordFrequency = wi.getFrequency();

		if (iWordFrequency == null) {
			iWordFrequency = 0;
		}

		Double logn = Math.log10(iWordFrequency.doubleValue() + 1d);

		Double logN = Math.log10(getCorpusSize() + 1d);

		return 1d - (logn / logN);
	}

	// FIXME - nova forma de pegar o IC.
	/*
	 * private static final ICFinder ic = ICFinder.getInstance(); private static
	 * final PathFinder pathfinder = new PathFinder(new NictWordNet());
	 * 
	 * private double informationContent(IWord wi) { POS pos =
	 * POS.valueOf(wi.getPos());
	 * 
	 * List<Synset> s = SynsetDAO.findSynsetsByNameAndPos(wi.getLemma(), pos);
	 * if (s.size() == 0) { return 0d; } Concept concept = new
	 * Concept(s.get(0).getSynset(), pos);
	 * 
	 * double inforContent = ic.ic(pathfinder, concept);
	 * 
	 * System.out.println("IC = " + inforContent);
	 * 
	 * return inforContent; }
	 */

	/**
	 * s(s1 |s2) = { A * [ sig(NN'1 | s2) + sig(VB'1 | s2) ] } + { B * [
	 * sig(ADJ'1 | s2) + sig(ADV'1 | s2)]}
	 * <p>
	 * Feng set A = 1.0 and B = 0.8
	 */
	private double fengStep3(double[] sum) {

		return (1.0d * (sum[0] + sum[1])) + (0.8d * (sum[2] + sum[3]));
	}

	private double getCorpusSize() {

		if (corpusSize == null) {
			corpusSize = wordFrequencyService.getCorpusSize();
		}
		return corpusSize.doubleValue();
	}
}
