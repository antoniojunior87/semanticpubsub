package br.uniriotec.fwbroker.semantic.nlp;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.uniriotec.fwbroker.entity.Word;
import br.uniriotec.fwbroker.util.WordFrequencyService;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;

@Component("NLPService")
public class NLPService {

	@Autowired
	private WordFrequencyService wordFrequencyService;

	private StanfordCoreNLP pipeline;

	private Dicionario dicionario;

	public NLPService() {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		// props.put("pos.model", "english-left3words-distsim.tagger");
		pipeline = new StanfordCoreNLP(props);
		dicionario = Dicionario.getInstance();
	}

	public Word[] getWords(String sentenca) {
		LinkedHashSet<Word> list = new LinkedHashSet<Word>();
		Word p = null;

		// read some text in the text variable
		String text = sentenca;
		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);
		// run all Annotators on this text
		pipeline.annotate(document);
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for (CoreMap sentence : sentences) {
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				p = new Word();
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				p.setPos(converterPOS(pos));
				// This is the lemma of the token
				String lemma = token.get(LemmaAnnotation.class).toLowerCase();
				p.setLemma(word);
				p.setIsSynonym(false);

				p.setFrequency(wordFrequencyService.getFrequency(lemma));

				// se é substantivo, verbo, adverbio ou adjetivo
				if (tipoPosValido(pos)) {
					/**
					 * Seguindo a proposta de FENG 2008 de enriquecer as
					 * palavras da sentenca com os sinonimos do primeiro Synset
					 */

					//LinkedHashSet<Word> listSinonimos = new LinkedHashSet<Word>();

					//IndexWord i = dicionario.obterIndexWord(pos, lemma);

					//if (i != null) {
					//	try {
					//		// primeiro Synset
					//		// Adiciona todos os sionimos
					//		for (net.didion.jwnl.data.Word w : i.getSenses()[0].getWords()) {

					//			String sPos = converterPOS(w.getPOS());
					//			String sLemma = w.getLemma().toLowerCase();

					//			Word sinonimo = new Word();
					//			sinonimo.setLemma(sLemma);
					//			sinonimo.setPos(sPos);
					//			sinonimo.setIsSynonym(true);

					//			sinonimo.setFrequency(wordFrequencyService.getFrequency(sLemma));

					//			listSinonimos.add(sinonimo);
					//		}
					//	} catch (Exception e) {
					//	}
					//}
					// adiciona a palavra a lista de palavras processsadas da
					// sentenca
					list.add(p);
					//list.addAll(listSinonimos);
				}
			}
		}
		return list.toArray(new Word[list.size()]);
	}

	private String converterPOS(POS pos) {

		if (POS.NOUN.getKey().equals(pos.getKey())) {
			return "n";
		} else if (POS.VERB.getKey().equals(pos.getKey())) {
			return "v";
		} else if (POS.ADJECTIVE.getKey().equals(pos.getKey())) {
			return "a";
		} else if (POS.ADVERB.getKey().equals(pos.getKey())) {
			return "r";
		}
		return null;
	}

	private String converterPOS(String pos) {

		if (pos.startsWith("NN")) {
			return "n";
		} else if (pos.startsWith("VB")) {
			return "v";
		} else if (pos.startsWith("JJ")) {
			return "a";
		} else if (pos.startsWith("RB")) {
			return "r";
		}
		return null;
	}

	private boolean tipoPosValido(String pos) {
		return (pos != null && pos.length() >= 2 && ("JJ".equals(pos.substring(0, 2))
				|| "VB".equals(pos.substring(0, 2)) || "NN".equals(pos.substring(0, 2)))
				|| "RB".equals(pos.substring(0, 2)));
	}

}
