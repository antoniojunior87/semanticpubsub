package br.uniriotec.fwbroker.semantic.similaridade;

import java.util.List;

import br.uniriotec.fwbroker.entity.Word;

public interface ISimilaridade {

	double calcularSimilaridade(List<Word> listPalavraOrigem, List<Word> listPalavraDestino);
}
