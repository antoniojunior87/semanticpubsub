package br.uniriotec.fwbroker.semantic.nlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;

public class Dicionario {

	private static final Dicionario dicionario = new Dicionario();
	private Dictionary dict;

	private Dicionario() {
		try {
			// Initialize WordNet - this must be done before you try
			// and create a similarity measure otherwise nasty things
			// might happen!
			JWNL.initialize(new FileInputStream(new File("/tmp/dict/wordnet.xml")));
			dict = Dictionary.getInstance();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (JWNLException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static Dicionario getInstance() {
		return dicionario;
	}

	public IndexWord obterIndexWord(String pos, String lema) {
		IndexWord word = null;

		try {
			if (pos.length() >= 2) {
				// Substantivo
				if (pos.substring(0, 2).equals("NN")) {
					word = dict.getIndexWord(POS.NOUN, lema);
				} // Verbo
				else if (pos.substring(0, 2).equals("VB")) {
					word = dict.getIndexWord(POS.VERB, lema);
				} // Adjetivo
				else if (pos.substring(0, 2).equals("JJ")) {
					word = dict.getIndexWord(POS.ADJECTIVE, lema);
				}
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return word;
	}

}
