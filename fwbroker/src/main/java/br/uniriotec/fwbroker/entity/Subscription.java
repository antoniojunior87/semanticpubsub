package br.uniriotec.fwbroker.entity;

import java.io.Serializable;

import br.uniriotec.broker.framework.common.ISubscription;

public class Subscription implements ISubscription, Serializable {

	private static final long serialVersionUID = 1L;

	private Constraint[] constraints;

	private String id;

	private String callback;

	private String originalSubscription;

	@Override
	public Constraint[] getConstraints() {
		return constraints;
	}

	public void setConstraints(Constraint[] constraints) {
		this.constraints = constraints;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getOriginalSubscription() {
		return originalSubscription;
	}

	public void setOriginalSubscription(String originalSubscription) {
		this.originalSubscription = originalSubscription;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(id);
		return builder.toString();
	}

}
