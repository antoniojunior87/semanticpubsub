package br.uniriotec.fwbroker.entity;

import java.io.Serializable;

import br.uniriotec.broker.framework.common.IAttribute;
import br.uniriotec.broker.framework.common.IWord;

public class Attribute implements IAttribute, Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private Word[] words;

	private Value value;

	public Attribute(String name, Word[] words) {
		this.name = name;
		this.words = words;
	}

	public Attribute(String name, Word[] words, Value value) {
		this(name, words);
		this.value = value;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWords(Word[] words) {
		this.words = words;
	}

	@Override
	public IWord[] getWords() {
		return words;
	}

	@Override
	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Attribute [");
		builder.append(name);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}

}
