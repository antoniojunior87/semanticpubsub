package br.uniriotec.fwbroker.entity;

import java.io.Serializable;

import br.uniriotec.broker.framework.common.IConstraint;
import br.uniriotec.broker.framework.common.IWord;
import br.uniriotec.broker.framework.common.Operator;

public class Constraint implements IConstraint, Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private Operator operator;

	private Word[] words;

	private Value value;

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWords(Word[] words) {
		this.words = words;
	}

	@Override
	public IWord[] getWords() {
		return words;
	}

	@Override
	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	@Override
	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Constraint [");
		builder.append(name);
		builder.append(", operator=");
		builder.append(operator);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}

}
