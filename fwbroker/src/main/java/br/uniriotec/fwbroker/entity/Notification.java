package br.uniriotec.fwbroker.entity;

import java.io.Serializable;

import br.uniriotec.broker.framework.common.INotification;

public class Notification implements INotification, Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String originalEvent;

	private Attribute[] attributes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOriginalEvent() {
		return originalEvent;
	}

	public void setOriginalEvent(String originalEvent) {
		this.originalEvent = originalEvent;
	}

	@Override
	public Attribute[] getAttributes() {
		return attributes;
	}

	public void setAttributes(Attribute[] attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(id);
		return builder.toString();
	}

}
