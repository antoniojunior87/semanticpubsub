package br.uniriotec.fwbroker.entity;

import java.io.Serializable;
import java.util.Date;

import br.uniriotec.broker.framework.common.IValue;
import br.uniriotec.broker.framework.common.ValueType;
import br.uniriotec.fwbroker.util.DateUtil;

public class Value implements IValue, Serializable {

	private static final long serialVersionUID = 1L;

	private ValueType type;

	private String sval;
	private long ival;
	private double dval;
	private boolean bval;

	public Value() {
		type = null;
	}

	public Value(Value x) {
		if (x == null) {
			type = null;
			return;
		}
		type = x.type;
		switch (type) {
		case INT:
		case LONG:
			ival = x.ival;
			break;
		case BOOL:
			bval = x.bval;
			break;
		case DOUBLE:
			dval = x.dval;
			break;
		case STRING:
		case DATE:
			sval = x.sval;
			break;
		default:
			break;
		}
	}

	public Value(String s) {
		if (s == null) {
			type = null;
			return;
		}

		if (DateUtil.canBeDate(s)) {
			type = ValueType.DATE;
		} else {
			type = ValueType.STRING;
		}
		sval = s;
	}

	public Value(int i) {
		type = ValueType.INT;
		ival = i;
	}

	public Value(long i) {
		type = ValueType.LONG;
		ival = i;
	}

	public Value(boolean b) {
		type = ValueType.BOOL;
		bval = b;
	}

	public Value(double d) {
		type = ValueType.DOUBLE;
		dval = d;
	}

	public Value(Date date) {
		type = ValueType.DATE;
		sval = DateUtil.toFormatedString(date);
	}

	@Override
	public ValueType getType() {
		return type;
	}

	@Override
	public int intValue() {
		switch (type) {
		case LONG:
		case INT:
			return (int) ival;
		case BOOL:
			return bval ? 1 : 0;
		case DOUBLE:
			return (int) dval;
		case STRING:
			return Integer.valueOf(sval).intValue();
		default:
			return 0;
		}
	}

	@Override
	public long longValue() {
		switch (type) {
		case LONG:
		case INT:
			return (long) ival;
		case BOOL:
			return bval ? 1 : 0;
		case DOUBLE:
			return (long) dval;
		case STRING:
			return Long.valueOf(sval).intValue();
		default:
			return 0;
		}
	}

	@Override
	public double doubleValue() {
		switch (type) {
		case LONG:
		case INT:
			return ival;
		case BOOL:
			return bval ? 1 : 0;
		case DOUBLE:
			return dval;
		case STRING:
			return Double.valueOf(sval).doubleValue();
		default:
			return 0;
		}
	}

	@Override
	public boolean booleanValue() {
		switch (type) {
		case LONG:
		case INT:
			return ival != 0;
		case BOOL:
			return bval;
		case DOUBLE:
			return dval != 0;
		case STRING:
			return Boolean.valueOf(sval).booleanValue();
		default:
			return false;
		}
	}

	@Override
	public String stringValue() {
		switch (type) {
		case LONG:
		case INT:
			return String.valueOf(ival);
		case BOOL:
			return String.valueOf(bval);
		case DOUBLE:
			return String.valueOf(dval);
		case DATE:
		case STRING:
			return sval;
		default:
			return "";
		}
	}

	@Override
	public Date dateValue() {

		switch (type) {
		case DATE:
		case STRING:
			return DateUtil.getDate(sval);
		default:
			return null;
		}
	}

	@Override
	public String toString() {
		return stringValue();
	}
}
