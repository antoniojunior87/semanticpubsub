package br.uniriotec.fwbroker.service;

import br.uniriotec.broker.framework.core.ISubscriptionHandler;

public interface SubscriptionService extends ISubscriptionHandler {

	public void removeSubscriptionById(String id);
}
