package br.uniriotec.fwbroker.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.ISubscription;
import br.uniriotec.fwbroker.entity.Subscription;
import br.uniriotec.fwbroker.service.SubscriptionService;

@Service("subscriptionService")
public class SubscriptionServiceImpl implements SubscriptionService {

	// TODO - Implementar DAO
	private Collection<ISubscription> subscriptions = new ArrayList<>();

	@Override
	public Collection<ISubscription> findAllSubscriptions() {
		// TODO - remover quando usar o DAO
		// cria uma copia para evitar problema de concorrencia ao inserir e ler
		return Arrays.asList(subscriptions.toArray(new ISubscription[0]));
	}

	@Override
	public void insertSubscription(ISubscription subscription) {
		subscriptions.add(subscription);
	}

	@Override
	public void removeSubscriptionById(String id) {

		// TODO - Chamar DAO
		for (ISubscription subscription : subscriptions) {
			if (((Subscription) subscription).getId().equals(id)) {
				subscriptions.remove(subscription);
			}
		}

	}

	@Override
	public void removeSubscription(ISubscription subscription) {
		// TODO Auto-generated method stub
		subscriptions.remove(subscription);
	}

}
