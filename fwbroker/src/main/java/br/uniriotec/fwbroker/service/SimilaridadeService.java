package br.uniriotec.fwbroker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.IAttribute;
import br.uniriotec.broker.framework.common.IWord;
import br.uniriotec.broker.framework.core.ISentenceSimilarity;
import br.uniriotec.broker.framework.core.ISimilarityStrategy;
import br.uniriotec.fwbroker.entity.Attribute;
import br.uniriotec.fwbroker.entity.Constraint;
import br.uniriotec.fwbroker.semantic.nlp.NLPService;
import br.uniriotec.fwbroker.service.impl.ConfiguredSimilarityService;
import br.uniriotec.fwbroker.util.Configuracao;

@Service
public class SimilaridadeService implements ISimilarityStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(SimilaridadeService.class);

	private static final double DEFAULT_THRESHOLD = 0.75d;

	@Autowired
	@Qualifier("averageSentenceSimilarity")
	private ISentenceSimilarity averageSentenceSimilarity;

	@Autowired
	@Qualifier("fengSentenceSimilarity")
	private ISentenceSimilarity fengSentenceSimilarity;

	@Autowired
	private NLPService nlpService;

	@Autowired
	private ConfiguredSimilarityService configuredSimilarityService;

	private Configuracao configuracao;

	@Autowired
	public SimilaridadeService(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	private double obterThresold() {
		return configuracao.getThreshold() != null ? configuracao.getThreshold().doubleValue() : DEFAULT_THRESHOLD;
	}

	private ISentenceSimilarity getSentenceSimilarity() {
		return configuracao.getSimilaridadeSentencas().toLowerCase().equals("feng") ? fengSentenceSimilarity
				: averageSentenceSimilarity;
	}

	@Cacheable(value = "similaridades", keyGenerator = "similaridadeKeyGenerator")
	@Override
	public boolean checkSimilarity(IAttribute subscriptionAttribute, IAttribute notificationAttribute) {

		if (configuredSimilarityService.isPreConfiguredSimilarity(subscriptionAttribute, notificationAttribute)) {
			return true;
		}

		Constraint sbAttribute = (Constraint) subscriptionAttribute;
		Attribute nbAttribute = (Attribute) notificationAttribute;

		return check(sbAttribute.getWords(), nbAttribute.getWords());
	}

	/**
	 * Metodo usado pelo Operador SS.
	 */
	@Override
	public boolean checkSimilarity(String subscriptionSentence, String notificationSentence) {

		if (configuredSimilarityService.isPreConfiguredSimilarity(subscriptionSentence, notificationSentence)) {
			return true;
		}

		IWord[] subscriptionWords = nlpService.getWords(subscriptionSentence);
		IWord[] notificationWords = nlpService.getWords(notificationSentence);

		double thresold = obterThresold();

		double similaridade = getSentenceSimilarity().computeSimilarity(subscriptionWords, notificationWords);

		LOG.trace("[{} >= {}][{}] - [{}][{}]", similaridade, thresold, (similaridade >= thresold), subscriptionWords,
				notificationWords);

		return similaridade >= thresold;
	}

	private boolean check(IWord[] subscriptionWords, IWord[] notificationWords) {

		double thresold = obterThresold();

		double similaridade = getSentenceSimilarity().computeSimilarity(subscriptionWords, notificationWords);

		LOG.trace("[{} >= {}][{}] - [{}][{}]", similaridade, thresold, (similaridade >= thresold), subscriptionWords,
				notificationWords);

		return similaridade >= thresold;
	}

	@Cacheable(value = "similaridades", keyGenerator = "similaridadeKeyGenerator")
	@Override
	public double getSimilarity(IAttribute subscriptionAttribute, IAttribute notificationAttribute) {

		if (configuredSimilarityService.isPreConfiguredSimilarity(subscriptionAttribute, notificationAttribute)) {
			return 1.0d;
		}

		Constraint sbAttribute = (Constraint) subscriptionAttribute;
		Attribute nbAttribute = (Attribute) notificationAttribute;

		return getSentenceSimilarity().computeSimilarity(sbAttribute.getWords(), nbAttribute.getWords());
	}
}
