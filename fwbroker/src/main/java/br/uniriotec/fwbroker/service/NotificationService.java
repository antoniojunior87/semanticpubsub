package br.uniriotec.fwbroker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.core.ICallbackHandler;
import br.uniriotec.broker.framework.core.ISimilarityStrategy;
import br.uniriotec.broker.framework.core.NotificationHandler;
import br.uniriotec.fwbroker.entity.Notification;
import br.uniriotec.fwbroker.util.Configuracao;
import br.uniriotec.fwbroker.util.HasanNotificationConverter;
import br.uniriotec.fwbroker.util.JsonNotificationConverter;
import br.uniriotec.fwbroker.util.LogPerformance;

@Service
public class NotificationService {

	private static final Logger LOG_PERF = LoggerFactory.getLogger(LogPerformance.NOME_LOG);

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private ICallbackHandler callbackService;

	@Autowired
	private ISimilarityStrategy similaridadeService;

	@Autowired
	private HasanNotificationConverter hasanNotificationConverter;

	@Autowired
	private JsonNotificationConverter jsonNotificationConverter;

	private NotificationHandler notificationHandler;

	private Configuracao configuracao;

	@Autowired
	public NotificationService(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	@Async
	public void deliverNotification(String event) throws Exception {
		Notification notification = converterNotification(event);
		LOG_PERF.debug(LogPerformance.TOKEN_EVENTO_RECEBIDO, System.currentTimeMillis(), notification);
		getNotificationHandler().deliverNotification(notification);
		LOG_PERF.debug(LogPerformance.TOKEN_EVENTO_PROCESSADO, System.currentTimeMillis(), notification);
	}

	public NotificationHandler getNotificationHandler() {

		if (notificationHandler == null) {
			notificationHandler = new NotificationHandler(callbackService, subscriptionService, similaridadeService,
					configuracao.getThreshold(), configuracao.getTipoChecagem());
		}

		return notificationHandler;
	}

	private Notification converterNotification(String event) throws Exception {
		switch (configuracao.getTipoEvento()) {
		case "hasan":
			return hasanNotificationConverter.newNotification(event);
		default:
			return jsonNotificationConverter.newNotification(event, configuracao.isConcatenarNomeNodeParent());
		}
	}

}
