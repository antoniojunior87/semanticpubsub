package br.uniriotec.fwbroker.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;

import br.uniriotec.broker.framework.common.INotification;
import br.uniriotec.broker.framework.common.ISubscription;
import br.uniriotec.broker.framework.core.ICallbackHandler;
import br.uniriotec.fwbroker.entity.Notification;
import br.uniriotec.fwbroker.entity.Subscription;
import br.uniriotec.fwbroker.util.LogPerformance;

@Service
public class CallbackHandler implements ICallbackHandler {

	// private static final String ID_EVENTO = "idEvento";

	// private static final Logger LOG =
	// LoggerFactory.getLogger(CallbackHandler.class);

	private static final Logger LOG_PERF = LoggerFactory.getLogger(LogPerformance.NOME_LOG);

	// private RestTemplate restTemplate;

	// private RestTemplate getRestTemplate() {
	// if (restTemplate != null) {
	// return restTemplate;
	// }
	// restTemplate = new RestTemplate();
	// return restTemplate;
	// }

	@Override
	public boolean callback(ISubscription subscription, INotification notification) {

		Notification bNotification = (Notification) notification;

		Subscription bSubscription = (Subscription) subscription;

		// FIXME desativado o callback para testar performance
		/*
		 * HttpHeaders headers = new HttpHeaders();
		 * headers.setContentType(MediaType.APPLICATION_JSON);
		 * headers.add(ID_EVENTO, bNotification.getId());
		 * 
		 * HttpEntity<String> entity = new
		 * HttpEntity<String>(bNotification.getEvent(), headers);
		 * 
		 * ResponseEntity<Void> response = null; try { response =
		 * getRestTemplate().postForEntity(bSubscription.getCallback(), entity,
		 * Void.class);
		 */

		LOG_PERF.debug(LogPerformance.TOKEN_EVENTO_ENVIADO_SUBS, System.currentTimeMillis(), bNotification.getId(),
				bSubscription.getId());

		/*
		 * LOG.debug("evento[{}] subscriber[{}]", bNotification.getId(),
		 * bSubscription.getId()); } catch (Exception e) {
		 * LOG.error("Erro ao tentar enviar evento para '{}'",
		 * bSubscription.getCallback()); return Boolean.FALSE; }
		 * 
		 * return HttpStatus.OK.equals(response.getStatusCode());
		 */
		return true;
	}
}
