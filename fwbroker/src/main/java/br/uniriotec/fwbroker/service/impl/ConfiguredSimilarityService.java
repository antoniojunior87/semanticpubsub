package br.uniriotec.fwbroker.service.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import br.uniriotec.broker.framework.common.IAttribute;

@Service
public class ConfiguredSimilarityService {

	@Value(value = "classpath:similarity-cache.conf")
	private Resource similarityCacheFile;

	private Set<String> preConfiguredSimilarities;

	private Set<String> getCache() {

		if (preConfiguredSimilarities == null) {
			preConfiguredSimilarities = new HashSet<>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(similarityCacheFile.getFile()));
				String line;
				while ((line = br.readLine()) != null) {
					preConfiguredSimilarities.add(line.toLowerCase());
				}
				br.close();
			} catch (Exception ex) {

			}
		}
		return preConfiguredSimilarities;
	}

	public boolean isPreConfiguredSimilarity(IAttribute subscriptionAttribute, IAttribute notificationAttribute) {

		String key1 = String.format("{%s=%s}", subscriptionAttribute.getName(), notificationAttribute.getName())
				.toLowerCase();
		String key2 = String.format("{%s=%s}", notificationAttribute.getName(), subscriptionAttribute.getName())
				.toLowerCase();

		return getCache().contains(key1) || getCache().contains(key2);
	}

	public boolean isPreConfiguredSimilarity(String subscriptionSentence, String notificationSentence) {

		String key1 = String.format("{%s=%s}", subscriptionSentence, notificationSentence).toLowerCase();
		String key2 = String.format("{%s=%s}", notificationSentence, subscriptionSentence).toLowerCase();

		return getCache().contains(key1) || getCache().contains(key2);
	}

}
