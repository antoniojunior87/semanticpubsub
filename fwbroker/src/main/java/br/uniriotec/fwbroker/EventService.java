
package br.uniriotec.fwbroker;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.uniriotec.broker.framework.core.Broker;
import br.uniriotec.fwbroker.entity.Subscription;
import br.uniriotec.fwbroker.service.NotificationService;
import br.uniriotec.fwbroker.service.SubscriptionService;
import br.uniriotec.fwbroker.util.Configuracao;
import br.uniriotec.fwbroker.util.HasanSubscriptionConverter;
import br.uniriotec.fwbroker.util.LogPerformance;
import br.uniriotec.fwbroker.util.SubscriptionConverter;

@RestController
@CrossOrigin
public class EventService implements Broker {

	private static final Logger LOG = LoggerFactory.getLogger(EventService.class);

	private static final Logger LOG_PERF = LoggerFactory.getLogger(LogPerformance.NOME_LOG);

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private HasanSubscriptionConverter hasanSubscriptionConverter;

	@Autowired
	private SubscriptionConverter subscriptionConverter;

	private Configuracao configuracao;

	@Autowired
	public EventService(Configuracao configuracao) {
		this.configuracao = configuracao;
		LOG_PERF.debug("{},{},{},{}", configuracao.getTipoEvento(),configuracao.getThreshold(),configuracao.getMetodoSimilaridade(),configuracao.getSimilaridadeSentencas());
	}

	@RequestMapping(path = "/connect", method = RequestMethod.GET, produces = "text/plain")
	public String connect() {
		String id = UUID.randomUUID().toString();
		LOG.info("new publisher joined - '{}'", id);
		return id;
	}

	@RequestMapping(path = "/subscribe", method = RequestMethod.POST, produces = "text/plain")
	public String subscribe(@RequestBody String terms, @RequestParam String callback) {
		Subscription subscription = converterSubscription(terms, callback);
		subscriptionService.insertSubscription(subscription);
		LOG.info("new subscriber joined - '{}'", subscription);
		LOG_PERF.debug(LogPerformance.TOKEN_NOVA_SUBSCRICAO, System.currentTimeMillis(), subscription);
		return subscription.getId();
	}

	@RequestMapping(path = "/unsubscribe/{id}", method = RequestMethod.GET, produces = "text/plain")
	public void unsubscribe(@PathVariable String id) {
		subscriptionService.removeSubscriptionById(id);
		LOG.info("subscriber left - ID: '{}'", id);
	}

	@RequestMapping(path = "/send", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void send(@RequestParam String id, @RequestBody String event) {
		LOG.info("new event received from publisher - ID: '{}', Event: '{}'", id, event);
		try {
			notificationService.deliverNotification(event);
		} catch (Exception e) {
			LOG.error("error - invalid event format");
			throw new IllegalArgumentException("error - invalid event format", e);
		}
	}

	private Subscription converterSubscription(String subscription, String callback) {

		switch (configuracao.getTipoEvento()) {
		case "hasan":
			return hasanSubscriptionConverter.newSubscription(subscription, callback);
		default:
			return subscriptionConverter.newSubscription(subscription, callback);
		}
	}

}