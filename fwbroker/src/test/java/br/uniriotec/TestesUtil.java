package br.uniriotec;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestesUtil {

	public static final String TERMOS_SMART_CITY = "termos/SmartCity";
	public static final String TERMOS_BASEBALL = "termos/Baseball";
	public static final String TERMOS_FACEBOOK = "termos/Facebook";
	public static final String TERMOS_GOOGLE_PLUS = "termos/GooglePlus";
	
	public static final String JSON_GOOGLE_PLUS_PEOPLE = "json/google_plus_people.json";

	public static List<String> obterTermosArquivo(String arquivo) throws IOException {

		String fileName = TestesUtil.class.getClassLoader().getResource(arquivo).getFile();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = null;
			List<String> termos = new ArrayList<>();
			while ((line = br.readLine()) != null) {
				termos.add(line);
			}
			return termos;
		}
	}

	public static String obterJsonArquivo(String arquivo) throws IOException {

		String fileName = TestesUtil.class.getClassLoader().getResource(arquivo).getFile();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = null;
			StringBuilder termos = new StringBuilder();
			while ((line = br.readLine()) != null) {
				termos.append(line);
			}
			return termos.toString();
		}
	}

}
