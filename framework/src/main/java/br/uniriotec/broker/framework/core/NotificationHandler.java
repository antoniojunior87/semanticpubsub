package br.uniriotec.broker.framework.core;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.uniriotec.broker.framework.common.INotification;
import br.uniriotec.broker.framework.common.ISubscription;

public class NotificationHandler {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationHandler.class.getName());

	private ICallbackHandler callbackHandler;

	private ISubscriptionHandler subscriptionHandler;

	private SubscriptionMatcher subscriptionMatcher;

	public NotificationHandler(ICallbackHandler callbackHandler, ISubscriptionHandler subscriptionHandler,
			ISimilarityStrategy similarityStrategy, Double threshold, String tipoChecagem) {
		this.callbackHandler = callbackHandler;
		this.subscriptionHandler = subscriptionHandler;
		this.subscriptionMatcher = new SubscriptionMatcher(similarityStrategy, threshold, tipoChecagem);
	}

	public void deliverNotification(INotification notification) {

		Collection<ISubscription> matchedSubscriptions = matchSubscriptions(notification);

		for (ISubscription subscription : matchedSubscriptions) {
			LOG.debug(String.format("invoking callback - '%s' '%s'", notification, subscription));
			callbackHandler.callback(subscription, notification);
		}
	}

	private Collection<ISubscription> matchSubscriptions(INotification notification) {

		Collection<ISubscription> allSubscriptions = subscriptionHandler.findAllSubscriptions();

		Collection<ISubscription> subscriptions = new ArrayList<ISubscription>();

		for (ISubscription subscription : allSubscriptions) {
			if (subscriptionMatcher.match(notification, subscription)) {
				subscriptions.add(subscription);
			}
		}

		return subscriptions;
	}
}
