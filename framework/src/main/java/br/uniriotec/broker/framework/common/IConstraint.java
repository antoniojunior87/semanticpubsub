package br.uniriotec.broker.framework.common;

/**
 * A constraint over an {@link IAttribute} in a {@link INotification}.
 * 
 * A constraint imposes a condition over the value of an attribute, and
 * implicitly on the existance of an attribute with the same name (or identified
 * by the similarity process).
 * <p>
 * A constraint is defined by a triple <em>(n,op,v)</em>, with a name
 * <em>n</em>, a comparison operator <em>op</em>, and a comparison value
 * <em>v</em>.
 * <p>
 * Such a constraint is satisfied by a Notification <em>m</em> if and only if
 * there exists an attribute <em>a</em> in <em>m</em> such that
 * <em>a.getName()</em> is 'similar' to <em>n</em>, and <em>a.getValue()</em>
 * matches <em>v</em> according to operator <em>op</em>.
 *
 * @see IValue
 **/
public interface IConstraint extends IAttribute {

	/**
	 * the comparison operator
	 * 
	 * valid values are defined in {@link Operator}
	 **/
	public Operator getOperator();
}