package br.uniriotec.broker.framework.common;

public interface IWord {

	public String getLemma();

	public String getPos();

	public Integer getFrequency();

	public boolean getIsSynonym();
}
