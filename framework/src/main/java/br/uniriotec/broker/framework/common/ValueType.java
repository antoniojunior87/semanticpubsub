package br.uniriotec.broker.framework.common;

/**
 * An <code>ValueType</code> can be of type <code>String</code>,
 * <code>int</code>, <code>long</code>, <code>double</code>,
 * <code>boolean</code>, <code>Date</code>.
 */
public enum ValueType {

	/**
	 * string of bytes
	 *
	 * corresponds to the Java <code>String</code> type.
	 **/
	STRING,

	/**
	 * integer type.
	 * 
	 * corresponds to the Java <code>long</code> type.
	 **/
	LONG,

	/**
	 * integer type.
	 *
	 * corresponds to the Java <code>int</code> type.
	 **/
	INT,

	/**
	 * double type.
	 *
	 * corresponds to the Java <code>double</code> type.
	 **/
	DOUBLE,

	/**
	 * boolean type.
	 *
	 * corresponds to the Java <code>boolean</code> type.
	 **/
	BOOL,

	/**
	 * date type.
	 * 
	 * corresponds to the Java <code>java.util.Date</code>.
	 */
	DATE;

	public boolean isString() {
		return STRING.equals(this);
	}

	public boolean isLong() {
		return LONG.equals(this);
	}

	public boolean isInteger() {
		return INT.equals(this);
	}

	public boolean isDouble() {
		return DOUBLE.equals(this);
	}

	public boolean isBoolean() {
		return BOOL.equals(this);
	}

	public boolean isDate() {
		return DATE.equals(this);
	}

}
