package br.uniriotec.broker.framework.common;

import java.util.Date;

/**
 * Value of an attribute in an event {@link INotification}.
 *
 * An {@link IAttribute} is a container for a typed vaule of an attribute in a
 * notification.
 * <p>
 * An <code>IValue</code> can be one of {@link ValueType} values.
 *
 **/
public interface IValue {

	public ValueType getType();

	public int intValue();

	public long longValue();

	public double doubleValue();

	public boolean booleanValue();

	public String stringValue();
	
	public Date dateValue();

}