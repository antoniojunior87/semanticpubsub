package br.uniriotec.broker.framework.core;

import br.uniriotec.broker.framework.common.IAttribute;

public interface ISimilarityStrategy {

	public boolean checkSimilarity(IAttribute subscriptionAttribute, IAttribute notificationAttribute);

	public double getSimilarity(IAttribute subscriptionAttribute, IAttribute notificationAttribute);

	public boolean checkSimilarity(String subscriptionSentence, String notificationSentence);
}
