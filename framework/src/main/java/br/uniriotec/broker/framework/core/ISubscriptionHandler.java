package br.uniriotec.broker.framework.core;

import java.util.Collection;

import br.uniriotec.broker.framework.common.ISubscription;

public interface ISubscriptionHandler {

	public Collection<ISubscription> findAllSubscriptions();

	public void insertSubscription(ISubscription subscription);

	public void removeSubscription(ISubscription subscription);
}
