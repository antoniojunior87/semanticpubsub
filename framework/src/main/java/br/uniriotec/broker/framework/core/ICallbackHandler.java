package br.uniriotec.broker.framework.core;

import br.uniriotec.broker.framework.common.INotification;
import br.uniriotec.broker.framework.common.ISubscription;

public interface ICallbackHandler {

	public boolean callback(ISubscription subscription, INotification notification);

}
