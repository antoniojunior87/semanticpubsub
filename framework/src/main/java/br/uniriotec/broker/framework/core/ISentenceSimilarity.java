package br.uniriotec.broker.framework.core;

import br.uniriotec.broker.framework.common.IWord;

public interface ISentenceSimilarity {

	public double computeSimilarity(IWord[] subscriptionWords, IWord[] notificationWords);

}
