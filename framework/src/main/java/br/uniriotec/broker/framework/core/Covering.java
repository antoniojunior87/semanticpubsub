package br.uniriotec.broker.framework.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.uniriotec.broker.framework.common.IAttribute;
import br.uniriotec.broker.framework.common.IConstraint;
import br.uniriotec.broker.framework.common.IValue;
import br.uniriotec.broker.framework.common.Operator;

/**
 * implementation of the covering relations between values.
 * <p>
 * The Operator ANY will aways result TRUE.
 * 
 */
public class Covering {

	private static final Logger LOG = LoggerFactory.getLogger(Covering.class.getName());

	private ISimilarityStrategy similarityStrategy;

	public Covering(ISimilarityStrategy similarityStrategy) {
		this.similarityStrategy = similarityStrategy;
	}

	public boolean applyOperator(IConstraint c, IAttribute x) {
		return applyOperator(x.getValue(), c.getOperator(), c.getValue());
	}

	public boolean applyOperator(IConstraint c, IValue x) {
		return applyOperator(x, c.getOperator(), c.getValue());
	}

	public boolean applyOperator(IAttribute x, Operator operator, IAttribute y) {
		return applyOperator(x.getValue(), operator, y.getValue());
	}

	public boolean applyOperator(IValue x, Operator operator, IValue y) {
		boolean result = applyOperatorMain(x, operator, y);
		LOG.trace("['{}' '{}' '{}'] = [{}]", x, operator, y, result);
		return result;
	}

	private boolean applyOperatorMain(IValue x, Operator operator, IValue y) {

		// x is the attribute of the event
		// y is the constraint of the subscribe

		if (operator == null) {
			return true;
		} else if (x == null && y == null) {
			return true;
		} else if (x == null && (operator != null && y != null)) {
			return false;
		}

		switch (operator) {
		case ANY:
			return true;
		case EQ:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().equals(y.stringValue());
			case BOOL:
				return y.getType().isBoolean() && x.booleanValue() == y.booleanValue();
			case INT:
			case LONG:
				return (y.getType().isInteger() && x.doubleValue() == y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() == y.doubleValue());
			case DOUBLE:
				return (y.getType().isInteger() && x.doubleValue() == y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() == y.doubleValue());
			case DATE:
				return (y.getType().isDate()) && (x.dateValue().equals(y.dateValue()));
			default:
				return false;
			}
		case NE:
			switch (x.getType()) {
			case STRING:
				return !y.getType().isString() || !x.stringValue().equals(y.stringValue());
			case BOOL:
				return !y.getType().isBoolean() || x.booleanValue() != y.booleanValue();
			case INT:
			case LONG:
				switch (y.getType()) {
				case INT:
				case LONG:
					return x.doubleValue() != y.doubleValue();
				case DOUBLE:
					return x.doubleValue() != y.doubleValue();
				default:
					return true;
				}
			case DOUBLE:
				switch (y.getType()) {
				case INT:
				case LONG:
					return x.doubleValue() != y.doubleValue();
				case DOUBLE:
					return x.doubleValue() != y.doubleValue();
				default:
					return true;
				}
			case DATE:
				return !(y.getType().isDate()) || !(x.dateValue().equals(y.dateValue()));
			default:
				return false;
			}
		case SS:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().indexOf(y.stringValue()) != -1;
			default:
				return false;
			}
		case SM:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && similarityStrategy.checkSimilarity(y.stringValue(), x.stringValue());
			default:
				return false;
			}
		case SF:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().endsWith(y.stringValue());
			default:
				return false;
			}
		case PF:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().startsWith(y.stringValue());
			default:
				return false;
			}
		case LT:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().compareTo(y.stringValue()) < 0;
			case INT:
			case LONG:
				return (y.getType().isInteger() && x.doubleValue() < y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() < y.doubleValue());
			case BOOL:
				return y.getType().isBoolean() && !x.booleanValue() && y.booleanValue();
			case DOUBLE:
				return (y.getType().isInteger() && x.doubleValue() < y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() < y.doubleValue());
			case DATE:
				return (y.getType().isDate()) && (x.dateValue().before(y.dateValue()));
			default:
				return false;
			}
		case GT:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().compareTo(y.stringValue()) > 0;
			case INT:
			case LONG:
				return (y.getType().isInteger() && x.doubleValue() > y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() > y.doubleValue());
			case BOOL:
				return y.getType().isBoolean() && x.booleanValue() && !y.booleanValue();
			case DOUBLE:
				return (y.getType().isInteger() && x.doubleValue() > y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() > y.doubleValue());
			case DATE:
				return (y.getType().isDate()) && (x.dateValue().after(y.dateValue()));
			default:
				return false;
			}
		case LE:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().compareTo(y.stringValue()) <= 0;
			case INT:
			case LONG:
				return (y.getType().isInteger() && x.doubleValue() <= y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() <= y.doubleValue());
			case BOOL:
				return y.getType().isBoolean() && (!x.booleanValue() || y.booleanValue());
			case DOUBLE:
				return (y.getType().isInteger() && x.doubleValue() <= y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() <= y.doubleValue());
			case DATE:
				return (y.getType().isDate())
						&& (x.dateValue().before(y.dateValue()) || x.dateValue().equals(y.dateValue()));
			default:
				return false;
			}
		case GE:
			switch (x.getType()) {
			case STRING:
				return y.getType().isString() && x.stringValue().compareTo(y.stringValue()) >= 0;
			case INT:
			case LONG:
				return (y.getType().isInteger() && x.doubleValue() >= y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() >= y.doubleValue());
			case BOOL:
				return y.getType().isBoolean() && (x.booleanValue() || !y.booleanValue());
			case DOUBLE:
				return (y.getType().isInteger() && x.doubleValue() >= y.doubleValue())
						|| (y.getType().isDouble() && x.doubleValue() >= y.doubleValue());
			case DATE:
				return (y.getType().isDate())
						&& (x.dateValue().after(y.dateValue()) || x.dateValue().equals(y.dateValue()));
			default:
				return false;
			}
		default:
			return false;
		}
	}

}
