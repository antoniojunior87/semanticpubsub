package br.uniriotec.broker.framework.common;

public interface IAttribute {

	/**
	 * returns the name of this attribute
	 */
	public String getName();

	/**
	 * returns the value of this attribute.
	 *
	 * @see IValue
	 */
	public IValue getValue();

	public IWord[] getWords();
}