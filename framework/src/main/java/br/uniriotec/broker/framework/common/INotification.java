package br.uniriotec.broker.framework.common;

/**
 * an event notification is simply a array of {@link IAttribute}.
 */
public interface INotification {

	public IAttribute[] getAttributes();
}
