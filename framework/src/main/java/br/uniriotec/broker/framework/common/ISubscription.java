package br.uniriotec.broker.framework.common;

/**
 * a Subscription is simply a array of {@link IConstraint}.
 **/
public interface ISubscription {

	public IConstraint[] getConstraints();

}
