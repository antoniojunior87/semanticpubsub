package br.uniriotec.broker.framework.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

import br.uniriotec.broker.framework.common.IAttribute;
import br.uniriotec.broker.framework.common.IConstraint;
import br.uniriotec.broker.framework.common.INotification;
import br.uniriotec.broker.framework.common.ISubscription;

public class SubscriptionMatcher {

	private ISimilarityStrategy similarityStrategy;

	private Covering covering;

	private Double threshold;

	private TipoChecagem tipoChecagem;

	public SubscriptionMatcher(ISimilarityStrategy similarityStrategy, Double threshold, String tipoChecagem) {
		this.similarityStrategy = similarityStrategy;
		this.covering = new Covering(similarityStrategy);
		this.threshold = threshold;
		this.tipoChecagem = "or".equals(tipoChecagem.toLowerCase()) ? TipoChecagem.OR : TipoChecagem.AND;
	}

	public boolean match(INotification notification, ISubscription subscription) {

		return check(subscription.getConstraints(), notification.getAttributes());
	}

	private boolean check(IConstraint[] subscriptionConstraints, IAttribute[] notificationAttributes) {

		// ordenar as listas antes do processamento
		// para evitar que a ordem aleatoria interfira no resultado

		Arrays.sort(subscriptionConstraints, new Comparator<IConstraint>() {
			public int compare(IConstraint o1, IConstraint o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		Arrays.sort(notificationAttributes, new Comparator<IAttribute>() {
			public int compare(IAttribute o1, IAttribute o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		Iterator<IConstraint> subs = new ArrayList<IConstraint>(Arrays.asList(subscriptionConstraints)).iterator();
		Iterator<IAttribute> attrs = new ArrayList<IAttribute>(Arrays.asList(notificationAttributes)).iterator();

		LinkedList<ParSimilar> similares = new LinkedList<SubscriptionMatcher.ParSimilar>();

		for (Iterator<IConstraint> sIterator = subs; sIterator.hasNext();) {

			IConstraint sConstraint = (IConstraint) sIterator.next();

			Double maxSimilarity = 0d;
			IAttribute selectedAttribute = null;

			for (Iterator<IAttribute> nIterator = attrs; nIterator.hasNext();) {

				IAttribute nAttribute = (IAttribute) nIterator.next();

				Double similarity = similarityStrategy.getSimilarity(sConstraint, nAttribute);

				if (similarity >= 1d) {
					// similaridade maxima
					maxSimilarity = similarity;
					selectedAttribute = nAttribute;
					// vai direto pro proximo contraint
					break;
				}

				// seleciona o attibute com maior similaridade e que a
				// similaridade seja maior que o threshold
				if (similarity >= threshold && similarity > maxSimilarity) {
					maxSimilarity = similarity;
					selectedAttribute = nAttribute;
				}
			}

			// houve attibute similar?
			if (selectedAttribute != null) {
				// elemento com maior similaridade selecionado
				similares.add(new ParSimilar(sConstraint, selectedAttribute, maxSimilarity));

				// remove o atributo
				attrs.remove();
			}

		}

		// todos os contraints devem ter um atributo correspondente
		// if (subscriptionConstraints.length != similares.size()) {
		// return false;
		// }

		return checkOperator(similares);
	}

	private boolean checkOperator(LinkedList<ParSimilar> similares) {

		if (similares.isEmpty()) {
			return false;
		}

		// tipo de checagem: && ou || entre os pares

		if (TipoChecagem.AND.equals(tipoChecagem)) {
			boolean resultadoFinal = true;
			for (ParSimilar par : similares) {
				resultadoFinal &= covering.applyOperator(par.constraint, par.attribute);
			}
			return resultadoFinal;
		} else {
			boolean resultadoFinal = false;
			for (ParSimilar par : similares) {
				resultadoFinal |= covering.applyOperator(par.constraint, par.attribute);
			}
			return resultadoFinal;
		}
	}

	private enum TipoChecagem {
		AND, OR;
	}

	private class ParSimilar {
		IConstraint constraint;
		IAttribute attribute;
		Double similarity;

		public ParSimilar(IConstraint constraint, IAttribute attribute, Double similarity) {
			super();
			this.constraint = constraint;
			this.attribute = attribute;
			this.similarity = similarity;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("[");
			builder.append(constraint.getName());
			builder.append(", ");
			builder.append(attribute.getName());
			builder.append(", ");
			builder.append(similarity);
			builder.append("]");
			return builder.toString();
		}

	}

}
