package br.uniriotec.broker.framework.common;

public enum Operator {

	/** equality operator */
	EQ,

	/** less than operator */
	LT,

	/** greater than operator */
	GT,

	/** greater o equal operator */
	GE,

	/** less or equal operator */
	LE,

	/**
	 * has prefix operator (for strings only, e.g., "software" PF "soft")
	 * 
	 * <em>x Operator.PF y</em> iff <em>x</em> begins with the prefix <em>y</em>
	 */
	PF,

	/**
	 * has suffix operator (for strings only, e.g., "software" SF "ware")
	 * 
	 * <em>x Operator.SF y</em> iff <em>x</em> ends with the suffix <em>y</em>
	 */
	SF,

	/** <em>any</em> operator */
	ANY,

	/** not equal operator */
	NE,

	/**
	 * substring operator (for strings only, e.g., "software" SS "war")
	 * 
	 * <em>x Operator.SS y</em> iff <em>x</em> contains the substring <em>y</em>
	 */
	SS,

	/**
	 * similarity operator (for strings only, e.g., "software" SM "program")
	 * 
	 * <em>x Operator.SM y</em> iff <em>x</em> is a string similar to <em>y</em>
	 */
	SM;

}
