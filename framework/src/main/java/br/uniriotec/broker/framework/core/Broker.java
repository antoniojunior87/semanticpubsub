package br.uniriotec.broker.framework.core;

public interface Broker {

	/**
	 * Publishers connection endpoint
	 * 
	 * @return publisher identifier
	 */
	public String connect();

	/**
	 * Subscribers subscription endpoint
	 * 
	 * @param subscription
	 *            in defined subscription language
	 * @param callback
	 *            - The subscriber's callback URL where notifications should be
	 *            delivered.
	 * @return Subscriber identifier
	 */
	public String subscribe(String subscription, String callback);

	/**
	 * Subscribers unsubscription endpoint
	 * 
	 * @param id
	 *            - Subscriber identifier
	 */
	public void unsubscribe(String id);

	/**
	 * The endpoint to Publishers publish events
	 * 
	 * @param id
	 *            - publisher identifier
	 * @param event
	 *            - data of the event
	 */
	public void send(String id, String event);
}
