#!/usr/bin/env bash

########################################################
# Script para configuracao e execucao dos experimentos #
########################################################

echo
echo "-------------------------------------------------------------------------------"
echo
echo "                  Script de configuracao de Experimentos"
echo
echo "-------------------------------------------------------------------------------"
echo
echo

###################################
## Limpa o arquivo de log do broker
truncate -s 0 "/tmp/spring/performance/broker.log"

#############
# Variaveis #
#############
BROKER_JAR_NAME=fwbroker-0.0.1-SNAPSHOT.jar
PUBLISHER_JAR_NAME=publisher-0.0.1-SNAPSHOT.jar
SUBSCRIBER_JAR_NAME=subscriber-0.0.1-SNAPSHOT.jar
LOG_JAR_NAME=loganalyzer-0.0.1-SNAPSHOT.jar

TMP_DIR="/tmp/experimento"
mkdir -p "$TMP_DIR"

###########
# Funcoes #
###########
executarJars()
{
	gnome-terminal --command="java -jar $TMP_DIR/$BROKER_JAR_NAME"
	sleep 10s
	gnome-terminal --command="java -jar $TMP_DIR/$SUBSCRIBER_JAR_NAME"
	sleep 8s
	gnome-terminal --command="java -jar $TMP_DIR/$PUBLISHER_JAR_NAME"

	echo
	echo "java -jar $TMP_DIR/$LOG_JAR_NAME"
}


## 0) Finaliza os processos anteriores, caso existam
BROKER_PS=$(ps -ef | grep -v grep | grep $BROKER_JAR_NAME | awk -F" " '{print $2}')
if [[ ! -z $BROKER_PS ]] ; then
	echo "Matando processo do Broker ['$BROKER_PS']"
	kill -9 $BROKER_PS
fi

PUBLISHER_PS=$(ps -ef | grep -v grep | grep $PUBLISHER_JAR_NAME | awk -F" " '{print $2}')
if [[ ! -z $PUBLISHER_PS ]] ; then
	echo "Matando processo do Publisher ['$PUBLISHER_PS']"
	kill -9 $PUBLISHER_PS
fi

SUBSCRIBER_PS=$(ps -ef | grep -v grep | grep $SUBSCRIBER_JAR_NAME | awk -F" " '{print $2}')
if [[ ! -z $SUBSCRIBER_PS ]] ; then
	echo "Matando processo do Subscriber ['$SUBSCRIBER_PS']"
	kill -9 $SUBSCRIBER_PS
fi


#########################################################
## Se informar parametro -e deve apenas executar o experimento
if [[ ! -z $1 ]] ; then
	echo
	echo "Executando experimento pre-configurado."
	echo
	executarJars $TMP_DIR $BROKER_JAR_NAME $SUBSCRIBER_JAR_NAME $PUBLISHER_JAR_NAME $LOG_JAR_NAME
	exit
fi

#########################################################

## 1) Configura o diretorio onde estao os arquivos do projeto

# obtem o diretorio onde o script esta
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
DIR="$SCRIPTPATH"

#read -p "Informe diretorio do projeto ['$SCRIPTPATH']:" ASK_DIR
# se nao informar nada, usa o SCRIPTPATH
#if [[ $ASK_DIR -ne "" ]]; then
#	DIR=$ASK_DIR
#fi
echo $SCRIPTPATH
#########################################################

## 2) configurar experimento

BROKER_DIR="$DIR/fwbroker"
BROKER_JAR="$BROKER_DIR/target/$BROKER_JAR_NAME"

### pergunta tipo de Dataset
DS_SUFIX="hasan"
#read -p "Informe o tipo de Dataset ['$DS_SUFIX']:" ASK_DS
#if [[ $ASK_DS -ne "" ]]; then
#	DS_SUFIX=$ASK_DIR
#fi
echo $DS_SUFIX
### Altera arquivo de configuracao do Broker
sed -i "/fwbroker.tipo-evento=*/c\fwbroker.tipo-evento=$DS_SUFIX" "$BROKER_DIR/src/main/resources/application.properties"
DS_SUFIX=".$DS_SUFIX"

### pergunda o Threshold
THRESHOLD="0.00"
read -p "Informe o Threshold ['$THRESHOLD']:" ASK_THRESHOLD
if [[ ! -z $ASK_THRESHOLD ]]; then
	THRESHOLD=$ASK_THRESHOLD
fi
### Altera arquivo de configuracao do Broker
sed -i "/fwbroker.threshold=*/c\fwbroker.threshold=$THRESHOLD" "$BROKER_DIR/src/main/resources/application.properties"

### pergunta metodo de similaridade
METODO="jiang"
#read -p "Informe o metodo de similaridade (opcoes: 'wup', 'lin', 'resnik', 'jiang') ['$METODO']:" ASK_METODO
#if [[ ! -z $ASK_METODO ]]; then
#	METODO=$ASK_METODO
#fi
echo $METODO
### Altera arquivo de configuracao do Broker
sed -i "/fwbroker.metodo-similaridade=*/c\fwbroker.metodo-similaridade=$METODO" "$BROKER_DIR/src/main/resources/application.properties"

### pergunta metodo de similaridade entre sentencas
METODO_SENTENCAS="feng"
#read -p "Informe o metodo de similaridade entre sentencas (opcoes: 'feng', 'average') ['$METODO_SENTENCAS']:" ASK_METODO_SENTENCAS
#if [[ ! -z $ASK_METODO_SENTENCAS ]]; then
#	METODO_SENTENCAS=$ASK_METODO_SENTENCAS
#fi
echo $METODO_SENTENCAS
### Altera arquivo de configuracao do Broker
sed -i "/fwbroker.similaridade-sentencas=*/c\fwbroker.similaridade-sentencas=$METODO_SENTENCAS" "$BROKER_DIR/src/main/resources/application.properties"

echo
echo
echo "-------------------------------------------------------------------------------"
echo "Compilando componentes. Aguarde ..."
echo
### Recompila o Broker
cd $BROKER_DIR
mvn -q -DskipTests clean install

echo "Broker compilado."
echo
echo

#########################################################

## 3) Verifica se os outros JAR necessarios existem

SUBSCRIBER_DIR="$DIR/subscriber"
SUBSCRIBER_JAR="$SUBSCRIBER_DIR/target/$SUBSCRIBER_JAR_NAME"

PUBLISHER_DIR="$DIR/publisher"
PUBLISHER_JAR="$PUBLISHER_DIR/target/$PUBLISHER_JAR_NAME"

LOG_DIR="$DIR/loganalyzer"
LOG_JAR="$LOG_DIR/target/$LOG_JAR_NAME"

if [ -f $SUBSCRIBER_JAR ] && [ -f $PUBLISHER_JAR ] && [ -f $LOG_JAR ]; then
	echo "Arquivos JAR encontrados"
else
	echo "Arquivos JAR NAO encontrados!"
	echo "Tentando compilar os jars"

	cd $SUBSCRIBER_DIR
	mvn -q -DskipTests clean install

	cd $PUBLISHER_DIR
	mvn -q -DskipTests clean install

	cd $LOG_DIR
	mvn -q -DskipTests clean install

	if [ -f $SUBSCRIBER_JAR ] && [ -f $PUBLISHER_JAR ] && [ -f $LOG_JAR ]; then
		echo "Arquivos JAR compilados"
	else
		echo "ERRO: Arquivos JAR NAO encontrados!"
		exit 1
	fi
fi

echo
echo
echo "-------------------------------------------------------------------------------"
echo
echo

#########################################################

## 4) verificar se os Datasets existem

SUBS_DS_NAME="subscriptions$DS_SUFIX"
SUBS_DS="$DIR/subscriber/src/main/resources/assinaturas/$SUBS_DS_NAME"
EVENTS_DS_NAME="events$DS_SUFIX"
EVENTS_DS="$DIR/publisher/src/main/resources/eventos/$EVENTS_DS_NAME"
RELEVANCE_DS_NAME="relevance-map$DS_SUFIX"
RELEVANCE_DS="$DIR/loganalyzer/src/main/resources/relevance_map/$RELEVANCE_DS_NAME"

if [ -f $SUBS_DS ] && [ -f $EVENTS_DS ] && [ -f $RELEVANCE_DS ]; then
	echo "Arquivos do DATASET encontrados!"
else
	echo "ERRO: Arquivos do DATASET NAO encontrados!"
	exit 1
fi

#########################################################

## 5) Copia arquivos para as pastas temporarias

# arquivos Jar
cp $BROKER_JAR $TMP_DIR
cp $SUBSCRIBER_JAR $TMP_DIR
cp $PUBLISHER_JAR $TMP_DIR
cp $LOG_JAR $TMP_DIR

# arquivos do Dataset
cp $SUBS_DS "/tmp/"
mv "/tmp/$SUBS_DS_NAME" "/tmp/subscriptions.tsv"
cp $EVENTS_DS "/tmp/"
mv "/tmp/$EVENTS_DS_NAME" "/tmp/events.tsv"
cp $RELEVANCE_DS "/tmp/"
mv "/tmp/$RELEVANCE_DS_NAME" "/tmp/relevance-map.tsv"

#########################################################

echo
echo
echo
echo "-------------------------------------------------------------------------------"
echo
echo "Experimento configurado com sucesso!"
echo
echo "-------------------------------------------------------------------------------"
echo
echo

#read -p "Deseja executar o Experimento?  [S/n]:" ASK_EXEC
# se nao informar nada, usa o padrao S
#if [ -z $ASK_EXEC ] || [ $ASK_EXEC -eq "s" ] || [ $ASK_EXEC -eq "S" ]; then
	executarJars $TMP_DIR $BROKER_JAR_NAME $SUBSCRIBER_JAR_NAME $PUBLISHER_JAR_NAME $LOG_JAR_NAME
#else
#	echo
#	echo "-------------------------------------------------------------------------------"
#	echo
#	echo "Comandos para executar experimento:" 
#	echo
#	echo "java -jar $TMP_DIR/$BROKER_JAR_NAME"
#	echo "java -jar $TMP_DIR/$SUBSCRIBER_JAR_NAME"
#	echo "java -jar $TMP_DIR/$PUBLISHER_JAR_NAME"
#	echo "java -jar $TMP_DIR/$LOG_JAR_NAME"
#	echo
#	echo
#fi