package br.uniriotec;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("publisher")
public class Configuracao {

	private String urlBroker;

	private String tipoPublisher;

	private Boolean usarProxy;

	private String proxyHost;

	private String proxyPort;

	public String getUrlBroker() {
		return urlBroker;
	}

	public void setUrlBroker(String urlBroker) {
		this.urlBroker = urlBroker;
	}

	public String getTipoPublisher() {
		return tipoPublisher;
	}

	public void setTipoPublisher(String tipoPublisher) {
		this.tipoPublisher = tipoPublisher;
	}

	public Boolean getUsarProxy() {
		return usarProxy;
	}

	public void setUsarProxy(Boolean usarProxy) {
		this.usarProxy = usarProxy;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

}