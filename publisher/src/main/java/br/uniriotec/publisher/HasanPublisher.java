package br.uniriotec.publisher;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import br.uniriotec.RestProxyTemplate;

public class HasanPublisher extends AbstractPublisher {

	private List<String> listaFontes;

	private static int sourceIndex = 0;

	public HasanPublisher(RestProxyTemplate restTemplate) {
		super(restTemplate);
		listaFontes = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("/tmp/events.tsv"));
			String line;

			while ((line = br.readLine()) != null) {
				listaFontes.add(line);
			}
			br.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String obterConteudoParaPublicar() {
		if (sourceIndex == obterListaFontes().size()) {
			return null;
		}

		return obterListaFontes().get(sourceIndex++);
	}

	private List<String> obterListaFontes() {
		return listaFontes;
	}
}
