package br.uniriotec.publisher;

import br.uniriotec.RestProxyTemplate;

public class HistoricalEventsPublisher extends AbstractPublisher {

	// http://www.vizgr.org/historical-events/search.php?format=json&begin_date=20000101&end_date=20170730&lang=en

	public HistoricalEventsPublisher(RestProxyTemplate restTemplate) {
		super(restTemplate);
	}

	public String obterConteudoParaPublicar() {

		String url = "http://www.vizgr.org/historical-events/search.php?format=json&begin_date=20000101&end_date=20170730&lang=en";
		String response = getRestTemplate().getForObject(url, String.class);
		return response;
	}
}
