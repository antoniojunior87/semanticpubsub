package br.uniriotec.publisher;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import br.uniriotec.RestProxyTemplate;

public abstract class AbstractPublisher {

	private RestProxyTemplate restTemplate;

	protected abstract String obterConteudoParaPublicar();

	public AbstractPublisher(RestProxyTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String publicar(String url) {

		String json = obterConteudoParaPublicar();

		if (json == null) {
			return null;
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(json, headers);

		getRestTemplate().postForObject(url, entity, String.class);
		return "";
	}

	protected RestProxyTemplate getRestTemplate() {
		return restTemplate;
	}
}
