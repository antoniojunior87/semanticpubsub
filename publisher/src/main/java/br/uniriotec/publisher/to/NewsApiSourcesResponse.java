package br.uniriotec.publisher.to;

import java.util.List;

public class NewsApiSourcesResponse {

	private List<NewsApiSource> sources;

	public List<NewsApiSource> getSources() {
		return sources;
	}

	public void setSources(List<NewsApiSource> sources) {
		this.sources = sources;
	}

}
