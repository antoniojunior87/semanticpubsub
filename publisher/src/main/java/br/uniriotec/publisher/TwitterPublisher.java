package br.uniriotec.publisher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.uniriotec.RestProxyTemplate;
import twitter4j.Query;
import twitter4j.Query.ResultType;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterPublisher extends AbstractPublisher {

	public TwitterPublisher(RestProxyTemplate restTemplate) {
		super(restTemplate);
	}

	private static final String TWITTER_CONSUMER_KEY = "flZ4kvMUMbnyXoL9zuXHU3cQv";
	private static final String TWITTER_SECRET_KEY = "dWhoHGTu66ePAZAzsMb6yYFOw1kyP688clMRhI1yd6zKNfl7lr";
	private static final String TWITTER_ACCESS_TOKEN = "39230868-G0a8UiXpuJmdrPWFYMXoU3hEFhOPNvLfcSsxGPMAF";
	private static final String TWITTER_ACCESS_TOKEN_SECRET = "kSyldi9FaObxJOoBJYmGswttDJWIUTRqIA86wJqiniVBB";

	private List<String> noticias;
	private static int index = 0;

	public static void main(String[] args) {
		new TwitterPublisher(null).obterNoticias();
	}

	private Collection<String> obterNoticias() {
		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setJSONStoreEnabled(true).setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
				.setOAuthConsumerSecret(TWITTER_SECRET_KEY).setOAuthAccessToken(TWITTER_ACCESS_TOKEN)
				.setOAuthAccessTokenSecret(TWITTER_ACCESS_TOKEN_SECRET);

		TwitterFactory tf = new TwitterFactory(cb.build());

		Twitter twitter = tf.getInstance();

		Query query = new Query("filter:news");
		query.setLang("en");
		query.setResultType(ResultType.mixed);
		query.setCount(100);
		query.setSince("2017-07-30");

		QueryResult result;

		Map<Long, String> noticias = new HashMap<>();

		do {
			try {
				result = twitter.search(query);
			} catch (TwitterException e) {
				return null;
			}

			List<Status> tweets = result.getTweets();

			for (Status tweet : tweets) {
				String json = TwitterObjectFactory.getRawJSON(tweet);
				noticias.put(tweet.getId(), tweet.getId() + "\t" + json);
				System.out.println(tweet.getId() + "\t" + json);
			}
		} while ((query = result.nextQuery()) != null);

		return noticias.values();

	}

	@Override
	protected String obterConteudoParaPublicar() {

		if (noticias == null) {
			noticias = new ArrayList<>(obterNoticias());
		}

		if (index == noticias.size()) {
			return null;
		}

		return noticias.get(index++);
	}

}
