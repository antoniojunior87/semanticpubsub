package br.uniriotec.publisher;

import java.util.ArrayList;
import java.util.List;

import br.uniriotec.RestProxyTemplate;
import br.uniriotec.publisher.to.NewsApiSource;
import br.uniriotec.publisher.to.NewsApiSourcesResponse;

public class NewsPublisher extends AbstractPublisher {

	private static final String API_KEY = "48f310bf400941fb9139a8d6d7ab4b24";
	private static final String URL_SOURCES = "https://newsapi.org/v1/sources";
	private static final String URL_ARTICLE = "https://newsapi.org/v1/articles?source=%s&apiKey=%s";

	private List<String> listaFontes;

	private static int sourceIndex = 0;

	public NewsPublisher(RestProxyTemplate restTemplate) {
		super(restTemplate);
	}

	public String obterConteudoParaPublicar() {
		if (sourceIndex == obterListaFontes().size()) {
			sourceIndex = 0;
		}

		String url = String.format(URL_ARTICLE, obterListaFontes().get(sourceIndex++), API_KEY);
		String response = getRestTemplate().getForObject(url, String.class);
		return response;
	}

	private List<String> obterListaFontes() {

		if (listaFontes == null) {

			listaFontes = new ArrayList<>();
			NewsApiSourcesResponse response = getRestTemplate().getForObject(URL_SOURCES, NewsApiSourcesResponse.class);

			for (NewsApiSource source : response.getSources()) {
				listaFontes.add(source.getId());
			}
		}
		return listaFontes;
	}
}
