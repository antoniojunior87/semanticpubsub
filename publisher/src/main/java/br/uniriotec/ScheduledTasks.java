package br.uniriotec;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.uniriotec.publisher.AbstractPublisher;
import br.uniriotec.publisher.HasanPublisher;
import br.uniriotec.publisher.HistoricalEventsPublisher;
import br.uniriotec.publisher.NewsPublisher;
import br.uniriotec.publisher.TwitterPublisher;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

	@Autowired
	private RestProxyTemplate restProxyTemplate;

	private Configuracao configuracao;

	private AbstractPublisher publisher;

	private String id;

	@Autowired
	public ScheduledTasks(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	@PostConstruct
	public void setUp() {

		if ("NewsPublisher".equals(configuracao.getTipoPublisher())) {
			publisher = new NewsPublisher(restProxyTemplate);
		} else if ("HasanPublisher".equals(configuracao.getTipoPublisher())) {
			publisher = new HasanPublisher(restProxyTemplate);
		} else if ("HistoricalEventsPublisher".equals(configuracao.getTipoPublisher())) {
			publisher = new HistoricalEventsPublisher(restProxyTemplate);
		} else if ("TwitterPublisher".equals(configuracao.getTipoPublisher())) {
			publisher = new TwitterPublisher(restProxyTemplate);
		}

		String urlConnect = configuracao.getUrlBroker() + "/connect";
		id = restProxyTemplate.getForObject(urlConnect, String.class);
	}

	@Scheduled(initialDelay = 200, fixedDelay = 1)
	public void publicar() {
		String urlSend = configuracao.getUrlBroker() + "/send?id=" + id;
		String idEvento = publisher.publicar(urlSend);

		if (idEvento == null) {
			System.exit(0);
			return;
		}

	//	log.info("{} - {}", dateFormat.format(new Date()), idEvento);
	}

}