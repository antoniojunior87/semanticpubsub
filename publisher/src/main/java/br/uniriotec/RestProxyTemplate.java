package br.uniriotec;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Component
public class RestProxyTemplate implements RestOperations {

	private RestTemplate restTemplate;

	private RestTemplate restTemplateProxy;

	private Configuracao configuracao;

	@Autowired
	public RestProxyTemplate(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	private RestTemplate getRestTemplate() {
		return getRestTemplate((URI) null);
	}

	private RestTemplate getRestTemplate(String url) {

		if (url == null) {
			return getRestTemplate((URI) null);
		}

		try {
			URI uri = new URI(url);
			return getRestTemplate(uri);
		} catch (URISyntaxException ex) {
			throw new IllegalArgumentException("Invalid URL after inserting base URL: " + url, ex);
		}
	}

	private RestTemplate getRestTemplate(URI url) {

		if (url == null || "localhost".equals(url.getHost())) {
			if (restTemplate != null) {
				return restTemplate;
			}
			restTemplate = new RestTemplate();
			return restTemplate;
		} else {
			if (restTemplateProxy != null) {
				return restTemplateProxy;
			}

			restTemplateProxy = new RestTemplate();

			if (configuracao.getUsarProxy()) {

				SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
				InetSocketAddress address = new InetSocketAddress(configuracao.getProxyHost(),
						Integer.parseInt(configuracao.getProxyPort()));
				Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
				factory.setProxy(proxy);
				restTemplateProxy.setRequestFactory(factory);
			}

			return restTemplateProxy;
		}

	}

	@Override
	public <T> T getForObject(String url, Class<T> responseType, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).getForObject(url, responseType, uriVariables);
	}

	@Override
	public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> uriVariables)
			throws RestClientException {
		return getRestTemplate(url).getForObject(url, responseType, uriVariables);
	}

	@Override
	public <T> T getForObject(URI url, Class<T> responseType) throws RestClientException {
		return getRestTemplate(url).getForObject(url, responseType);
	}

	@Override
	public <T> ResponseEntity<T> getForEntity(String url, Class<T> responseType, Object... uriVariables)
			throws RestClientException {
		return getRestTemplate(url).getForEntity(url, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> getForEntity(String url, Class<T> responseType, Map<String, ?> uriVariables)
			throws RestClientException {
		return getRestTemplate(url).getForEntity(url, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> getForEntity(URI url, Class<T> responseType) throws RestClientException {
		return getRestTemplate(url).getForEntity(url, responseType);
	}

	@Override
	public HttpHeaders headForHeaders(String url, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).headForHeaders(url, uriVariables);
	}

	@Override
	public HttpHeaders headForHeaders(String url, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).headForHeaders(url, uriVariables);
	}

	@Override
	public HttpHeaders headForHeaders(URI url) throws RestClientException {
		return getRestTemplate(url).headForHeaders(url);
	}

	@Override
	public URI postForLocation(String url, Object request, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).postForLocation(url, request, uriVariables);
	}

	@Override
	public URI postForLocation(String url, Object request, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).postForLocation(url, request, uriVariables);
	}

	@Override
	public URI postForLocation(URI url, Object request) throws RestClientException {
		return getRestTemplate(url).postForLocation(url, request);
	}

	@Override
	public <T> T postForObject(String url, Object request, Class<T> responseType, Object... uriVariables)
			throws RestClientException {
		return getRestTemplate(url).postForObject(url, request, responseType, uriVariables);
	}

	@Override
	public <T> T postForObject(String url, Object request, Class<T> responseType, Map<String, ?> uriVariables)
			throws RestClientException {
		return getRestTemplate(url).postForObject(url, request, responseType, uriVariables);
	}

	@Override
	public <T> T postForObject(URI url, Object request, Class<T> responseType) throws RestClientException {
		return getRestTemplate(url).postForObject(url, request, responseType);
	}

	@Override
	public <T> ResponseEntity<T> postForEntity(String url, Object request, Class<T> responseType,
			Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).postForEntity(url, request, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> postForEntity(String url, Object request, Class<T> responseType,
			Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).postForEntity(url, request, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> postForEntity(URI url, Object request, Class<T> responseType)
			throws RestClientException {
		return getRestTemplate(url).postForEntity(url, request, responseType);
	}

	@Override
	public void put(String url, Object request, Object... uriVariables) throws RestClientException {
		getRestTemplate(url).put(url, request, uriVariables);
	}

	@Override
	public void put(String url, Object request, Map<String, ?> uriVariables) throws RestClientException {
		getRestTemplate(url).put(url, request, uriVariables);
	}

	@Override
	public void put(URI url, Object request) throws RestClientException {
		getRestTemplate(url).put(url, request);
	}

	@Override
	public <T> T patchForObject(String url, Object request, Class<T> responseType, Object... uriVariables)
			throws RestClientException {
		return getRestTemplate(url).patchForObject(url, request, responseType, uriVariables);
	}

	@Override
	public <T> T patchForObject(String url, Object request, Class<T> responseType, Map<String, ?> uriVariables)
			throws RestClientException {
		return getRestTemplate(url).patchForObject(url, request, responseType, uriVariables);
	}

	@Override
	public <T> T patchForObject(URI url, Object request, Class<T> responseType) throws RestClientException {
		return getRestTemplate(url).patchForObject(url, request, responseType);
	}

	@Override
	public void delete(String url, Object... uriVariables) throws RestClientException {
		getRestTemplate(url).delete(url, uriVariables);
	}

	@Override
	public void delete(String url, Map<String, ?> uriVariables) throws RestClientException {
		getRestTemplate(url).delete(url, uriVariables);
	}

	@Override
	public void delete(URI url) throws RestClientException {
		getRestTemplate(url).delete(url);
	}

	@Override
	public Set<HttpMethod> optionsForAllow(String url, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).optionsForAllow(url, uriVariables);
	}

	@Override
	public Set<HttpMethod> optionsForAllow(String url, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).optionsForAllow(url, uriVariables);
	}

	@Override
	public Set<HttpMethod> optionsForAllow(URI url) throws RestClientException {
		return getRestTemplate(url).optionsForAllow(url);
	}

	@Override
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			Class<T> responseType, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			Class<T> responseType, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> exchange(URI url, HttpMethod method, HttpEntity<?> requestEntity,
			Class<T> responseType) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType);
	}

	@Override
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			ParameterizedTypeReference<T> responseType, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			ParameterizedTypeReference<T> responseType, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType, uriVariables);
	}

	@Override
	public <T> ResponseEntity<T> exchange(URI url, HttpMethod method, HttpEntity<?> requestEntity,
			ParameterizedTypeReference<T> responseType) throws RestClientException {
		return getRestTemplate(url).exchange(url, method, requestEntity, responseType);
	}

	@Override
	public <T> ResponseEntity<T> exchange(RequestEntity<?> requestEntity, Class<T> responseType)
			throws RestClientException {
		return getRestTemplate().exchange(requestEntity, responseType);
	}

	@Override
	public <T> ResponseEntity<T> exchange(RequestEntity<?> requestEntity, ParameterizedTypeReference<T> responseType)
			throws RestClientException {
		return getRestTemplate().exchange(requestEntity, responseType);
	}

	@Override
	public <T> T execute(String url, HttpMethod method, RequestCallback requestCallback,
			ResponseExtractor<T> responseExtractor, Object... uriVariables) throws RestClientException {
		return getRestTemplate(url).execute(url, method, requestCallback, responseExtractor, uriVariables);
	}

	@Override
	public <T> T execute(String url, HttpMethod method, RequestCallback requestCallback,
			ResponseExtractor<T> responseExtractor, Map<String, ?> uriVariables) throws RestClientException {
		return getRestTemplate(url).execute(url, method, requestCallback, responseExtractor, uriVariables);
	}

	@Override
	public <T> T execute(URI url, HttpMethod method, RequestCallback requestCallback,
			ResponseExtractor<T> responseExtractor) throws RestClientException {
		return getRestTemplate(url).execute(url, method, requestCallback, responseExtractor);
	}
}
