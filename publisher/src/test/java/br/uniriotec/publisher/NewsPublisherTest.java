package br.uniriotec.publisher;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class NewsPublisherTest {

	private NewsPublisher newsPublisher;

	@Before
	public void setUp() {
		// newsPublisher = new NewsPublisher(new RestTemplate());
	}

	@Test
	public void testaConexaoApi() {
		String json = newsPublisher.obterConteudoParaPublicar();
		System.out.println(json);
		Assert.assertNotNull(json);
	}

	@Test
	public void testaTrocaFonteApi() {
		String jsonAnterior = newsPublisher.obterConteudoParaPublicar();
		for (int i = 0; i < 69; i++) {
			String json = newsPublisher.obterConteudoParaPublicar();
			System.out.println(json);
			Assert.assertNotEquals(jsonAnterior, json);
		}
	}

}
