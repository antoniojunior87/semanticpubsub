#!/bin/bash
set -e

if [ "$SUBSCRIBER_TERMOS" ] && [ "$SUBSCRIBER_URL_BROKER" ]; then
	exec java -Djava.security.egd=file:/dev/./urandom -jar /app.jar --subscriber.urlBroker=$SUBSCRIBER_URL_BROKER --subscriber.termos=$SUBSCRIBER_TERMOS "$@"
else
	cat >&2 <<-'EOWARN'
		****************************************************************************************************************
		WARNING: URL do broker e termos nao especificados! Serao usados os valores padroes da aplicacao.
		         Use "-e SUBSCRIBER_URL_BROKER=http://servidor:porta SUBSCRIBER_TERMOS=x,y,z" to set it in "docker run".
		****************************************************************************************************************
	EOWARN
	exec java -Djava.security.egd=file:/dev/./urandom -jar /app.jar "$@"
fi