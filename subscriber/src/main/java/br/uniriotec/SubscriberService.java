
package br.uniriotec;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SubscriberService {

	private static final Logger LOG = LoggerFactory.getLogger(SubscriberService.class);

	private static final String URL = "/subscribe?callback=%s";

	@Inject
	private ServerProperties serverProperties;

	@Autowired
	private RestProxyTemplate restProxyTemplate;

	private ConfiguracaoSubscriber configuracao;

	//private String id;

	@Autowired
	public SubscriberService(ConfiguracaoSubscriber configuracao) {
		this.configuracao = configuracao;
	}

	@PostConstruct
	public void setUp() {

		// String urlSubscribe = configuracao.getUrlBroker() +
		// String.format(URL, getCallback());
		//
		// LOG.info("URL Subscribe: '{}'", urlSubscribe);
		//
		// id = restProxyTemplate.getRestTemplate().postForObject(urlSubscribe,
		// configuracao.getTermos(), String.class);
		// LOG.info("Subscriber ID: '{}'", id);

		try {
			conf();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void conf() throws FileNotFoundException, IOException {

		String urlSubscribe = configuracao.getUrlBroker() + String.format(URL, getCallback());

		try (BufferedReader br = new BufferedReader(new FileReader("/tmp/subscriptions.tsv"))) {
			String line;
			while ((line = br.readLine()) != null) {
				String id = restProxyTemplate.getRestTemplate().postForObject(urlSubscribe, line, String.class);
				LOG.info("Subscriber ID: '{}'", id);
			}
		}
	}

	@RequestMapping(path = "/receive", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public void receive(@RequestBody String event, @RequestHeader String idEvento) {
		LOG.info("'{}'-{}", idEvento, event);
	}

	private String getCallback() {

		String hostname;

		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = "localhost";
		}

		return String.format("http://%s:%d/receive", hostname, serverProperties.getPort());
	}
}