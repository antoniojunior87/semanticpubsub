package br.uniriotec;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("subscriber")
public class ConfiguracaoSubscriber {

	private String urlBroker;

	private Boolean usarProxy;

	private String proxyHost;

	private String proxyPort;

	private String termos;

	public String getUrlBroker() {
		return urlBroker;
	}

	public void setUrlBroker(String urlBroker) {
		this.urlBroker = urlBroker;
	}

	public Boolean getUsarProxy() {
		return usarProxy;
	}

	public void setUsarProxy(Boolean usarProxy) {
		this.usarProxy = usarProxy;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getTermos() {
		return termos;
	}

	public void setTermos(String termos) {
		this.termos = termos;
	}

}