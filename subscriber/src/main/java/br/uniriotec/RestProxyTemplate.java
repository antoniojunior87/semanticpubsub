package br.uniriotec;

import java.net.InetSocketAddress;
import java.net.Proxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestProxyTemplate {

	private RestTemplate restTemplate;

	private ConfiguracaoSubscriber configuracao;

	@Autowired
	public RestProxyTemplate(ConfiguracaoSubscriber configuracao) {
		this.configuracao = configuracao;
	}

	public RestTemplate getRestTemplate() {

		if (restTemplate != null) {
			return restTemplate;
		}

		restTemplate = new RestTemplate();

		if (configuracao.getUsarProxy()) {

			SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
			InetSocketAddress address = new InetSocketAddress(configuracao.getProxyHost(),
					Integer.parseInt(configuracao.getProxyPort()));
			Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
			factory.setProxy(proxy);
			restTemplate.setRequestFactory(factory);
		}

		return restTemplate;
	}
}