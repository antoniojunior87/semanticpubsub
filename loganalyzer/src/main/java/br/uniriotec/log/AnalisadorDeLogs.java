package br.uniriotec.log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalisadorDeLogs {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static void main(String[] args) throws Exception {

		// Chama a classe TratadorDeLogs para gerar o arquivo
		// /tmp/resultado.tsv
		String[] experimetParameter = TratadorDeLogs.tratarLog().split(",");

		ResultadoEficacia resultadoEficacia = EstatisticaDeEficacia.obterResultado();

		ResultadoPerformance resultadoPerformance = EstatisticaDePerformance.obterResultado();

		System.out.println(String.format("\n\n\nRelatório de execução de experimento\n\n Data do relatório:\t%s",
				SDF.format(new Date())));

		System.out.println(
				String.format(" Dataset:\t\t%s\n Threshold:\t\t%s\n Met. Similaridade:\t%s\n Met. Sim. Sent:\t%s\n",
						experimetParameter[0], experimetParameter[1], experimetParameter[2], experimetParameter[3]));

		System.out.println(String.format(
				" TruePositives:\t\t%d\n FalsePositives:\t%d\n FalseNegatives:\t%d\n\n Precision:\t\t%f\n Recall:\t\t%f\n F-Score:\t\t%f ",
				resultadoEficacia.getTruePositives(), resultadoEficacia.getFalsePositives(),
				resultadoEficacia.getFalseNegatives(), (resultadoEficacia.getPrecisao().movePointRight(2)),
				(resultadoEficacia.getCobertura().movePointRight(2)),
				(resultadoEficacia.getFscore().movePointRight(2))));

		System.out.println(String.format(" Throughput:\t\t%d (n/s)\n Taxa Notificação:\t%d (ms)",
				resultadoPerformance.getTaxaNotificacao(), resultadoPerformance.getTempoMedio()));

		System.out.println(String.format("Resultadp: %f\t%f\t%f", resultadoEficacia.getPrecisao().movePointRight(2),
				resultadoEficacia.getCobertura().movePointRight(2), resultadoEficacia.getFscore().movePointRight(2)));

		System.out.println("----------------------------------------");
	}

}
