package br.uniriotec.log;

import org.apache.commons.csv.CSVRecord;

/**
* Log de performance
*/
public class LogEvento implements ILogEvento {

	private static final String TIMESTAMP = "timestamp";
	private static final String EVENTO = "evento";
	private static final String TIME = "time";
	private static final String TOKEN = "token";
	public static final String[] HEADERS = { TIMESTAMP, EVENTO, TIME, TOKEN };

	private String timestamp;
	private String evento;
	private String time;
	private String token;

	public LogEvento(CSVRecord csvRecord) {
		super();
		timestamp = csvRecord.get(TIMESTAMP);
		evento = csvRecord.get(EVENTO);
		time = csvRecord.get(TIME);
		token = csvRecord.get(TOKEN);
	}

	public LogEvento(String timestamp, String evento, String time, String token) {
		super();
		this.timestamp = timestamp;
		this.evento = evento;
		this.time = time;
		this.token = token;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(timestamp);
		builder.append(" - ");
		builder.append(evento);
		builder.append(" - ");
		builder.append(time);
		builder.append(" - ");
		builder.append(token);
		return builder.toString();
	}

	public int compareTo(ILogEvento o) {

		LogEvento other = (LogEvento) o;

		int i = evento.compareTo(other.evento);
		if (i != 0) {
			return i;
		}
		return time.compareTo(other.time);
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getIdentificador() {
		return evento;
	}

}