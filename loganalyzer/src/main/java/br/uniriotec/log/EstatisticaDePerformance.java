package br.uniriotec.log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EstatisticaDePerformance {

	public static final String TOKEN_EVENTO_RECEBIDO = "tag_evento_recebido";
	public static final String TOKEN_EVENTO_PROCESSADO = "tag_evento_processado";
	public static final String TOKEN_NOVA_SUBSCRICAO = "tag_nova_subscricao";
	public static final String TOKEN_EVENTO_ENVIADO_SUBS = "tag_evento_enviado";

	public static ResultadoPerformance obterResultado() throws Exception {

		List<Linha> linhas = TratadorDeLogs.obterLinhas();

		Map<String, List<Linha>> map = new HashMap<String, List<Linha>>();

		// agrupa todas as linhas por evento
		for (Linha logEvento : linhas) {

			// ignora linhas das subscricoes
			if (logEvento.getIdEvento() == null) {
				continue;
			}

			List<Linha> eventosAgrupados = null;

			if (!map.containsKey(logEvento.getIdEvento())) {
				eventosAgrupados = new ArrayList<Linha>();
				map.put(logEvento.getIdEvento(), eventosAgrupados);
			} else {
				eventosAgrupados = map.get(logEvento.getIdEvento());
			}
			eventosAgrupados.add(logEvento);
		}

		Double count = 0d;
		Double somatorioTempo = 0d;

		Double maiorTempo = 0d;
		Double menorTempo = new Double(Long.MAX_VALUE);

		// calculo do tempo médio de notificação
		for (String key : map.keySet()) {

			List<Linha> logs = map.get(key);

			Collections.sort(logs, new Comparator<Linha>() {
				@Override
				public int compare(Linha o1, Linha o2) {
					return o1.getTime().compareTo(o2.getTime());
				}
			});

			Linha logEventoRecebido = logs.get(0);
			Linha logEventoProcessado = logs.get(logs.size() - 1);

			if (logEventoRecebido != null && logEventoProcessado != null) {

				count++;

				Long tempoProcessamentoEvento = logEventoProcessado.getTime() - logEventoRecebido.getTime();

				if (logEventoProcessado.getTime().doubleValue() > maiorTempo) {
					// mantem o tempo do ultimo evento processado
					maiorTempo = logEventoProcessado.getTime().doubleValue();
				}

				if (logEventoRecebido.getTime().doubleValue() < menorTempo) {
					// mantem o tempo do primeiro evento recebido
					menorTempo = logEventoRecebido.getTime().doubleValue();
				}

				somatorioTempo += tempoProcessamentoEvento;
			}
		}

		ResultadoPerformance resultado = new ResultadoPerformance();

		if (count > 0) {
			Double tempoMedioNotificacao = somatorioTempo / count;
			resultado.setTempoMedio(tempoMedioNotificacao);

			Double tempoTotalMilisegundos = (maiorTempo - menorTempo);
			Double tempoTotal = 1d;
			if (tempoTotalMilisegundos > 1000) {
				tempoTotal = tempoTotalMilisegundos / 1000;
			}

			Double taxaNotificacao = count / tempoTotal;
			resultado.setTaxaNotificacao(taxaNotificacao);
		}

		return resultado;
	}

	public static void main(String[] args) throws Exception {
		ResultadoPerformance resultadoPerformance = EstatisticaDePerformance.obterResultado();

		System.out.println(resultadoPerformance.getTempoMedio() + " (ms) | " + resultadoPerformance.getTaxaNotificacao()
				+ " (events/sec)");
	}
}
