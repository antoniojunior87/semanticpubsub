package br.uniriotec.log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dataset {

	public static void main(String[] args) throws Exception {

		List<String> idsEventos = obterEventos();
		List<String> idsAssinaturas = obterAssinaturas();

		Map<String, List<String>> mapa = obterRelevanceMap(idsAssinaturas, idsEventos);

		for (Map.Entry<String, List<String>> entry : mapa.entrySet()) {
			System.out.println(String.format("%s\t%s", entry.getKey(), entry.getValue()));
		}
	}

	private static List<String> obterEventos() throws IOException, FileNotFoundException {
		List<String> eventos = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader("/home/antonio/dataset/events.tsv"))) {
			String line;
			while ((line = br.readLine()) != null) {
				eventos.add(line.split("\t")[0].trim());
			}
		}
		return eventos;
	}

	private static List<String> obterAssinaturas() throws Exception {

		List<String> assinaturas = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader("/home/antonio/dataset/subscriptions.tsv"))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] partes = line.split("\t");
				assinaturas.add(partes[0].trim());
			}
		}
		return assinaturas;
	}

	private static Map<String, List<String>> obterRelevanceMap(List<String> idsAssinaturas, List<String> idsEventos)
			throws Exception {

		Map<String, List<String>> mapa = new HashMap<String, List<String>>();

		try (BufferedReader br = new BufferedReader(new FileReader("/home/antonio/dataset/relevance-map.tsv"))) {
			String line;
			while ((line = br.readLine()) != null) {

				String[] partes = line.split("\t");

				List<String> lista = new ArrayList<>();

				String idSubscricao = partes[0].trim();

				if (!idsAssinaturas.contains(idSubscricao)) {
					continue;
				}

				String[] valores = partes[1].replaceAll("\\[", "").replaceAll("\\]", "").split(", ");
				for (String idEvento : valores) {
					if (idsEventos.contains(idEvento)) {
						lista.add(idEvento);
					}
				}

				mapa.put(idSubscricao, lista);
			}
		}
		return mapa;
	}
}
