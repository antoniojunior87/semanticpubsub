package br.uniriotec.log;

import java.math.BigDecimal;

public class ResultadoEficacia {

	private Integer truePositives;
	private Integer falsePositives;
	private Integer falseNegatives;
	private BigDecimal precisao;
	private BigDecimal cobertura;
	private BigDecimal fscore;

	public Integer getTruePositives() {
		return truePositives;
	}

	public void setTruePositives(Integer truePositives) {
		this.truePositives = truePositives;
	}

	public Integer getFalsePositives() {
		return falsePositives;
	}

	public void setFalsePositives(Integer falsePositives) {
		this.falsePositives = falsePositives;
	}

	public Integer getFalseNegatives() {
		return falseNegatives;
	}

	public void setFalseNegatives(Integer falseNegatives) {
		this.falseNegatives = falseNegatives;
	}

	public BigDecimal getPrecisao() {
		return precisao;
	}

	public void setPrecisao(BigDecimal precisao) {
		this.precisao = precisao;
	}

	public BigDecimal getCobertura() {
		return cobertura;
	}

	public void setCobertura(BigDecimal cobertura) {
		this.cobertura = cobertura;
	}

	public BigDecimal getFscore() {
		return fscore;
	}

	public void setFscore(BigDecimal fscore) {
		this.fscore = fscore;
	}

}