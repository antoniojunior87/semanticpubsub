package br.uniriotec.log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TratadorDeLogs {

	public static final String TOKEN_EVENTO_RECEBIDO = "tag_evento_recebido";
	public static final String TOKEN_EVENTO_PROCESSADO = "tag_evento_processado";
	public static final String TOKEN_NOVA_SUBSCRICAO = "tag_nova_subscricao";
	public static final String TOKEN_EVENTO_ENVIADO_SUBS = "tag_evento_enviado";

	private static final String resultado = "/tmp/resultado.tsv";

	private static final String logBroker = "/tmp/spring/performance/broker.log";

	public static String tratarLog() throws Exception {

		List<Linha> linhas = new ArrayList<>();

		int countLine = 1;
		String experimetParameter = null;

		try (BufferedReader br = new BufferedReader(new FileReader(logBroker))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (countLine == 1) {
					experimetParameter = line;
				} else {
					linhas.add(mapeiaLinha(line.split(",")));
				}
				countLine++;
			}
		}

		criarMapaSubscricao(linhas);

		return experimetParameter;
	}

	public static List<Linha> obterLinhas() throws Exception {

		List<Linha> linhas = new ArrayList<>();

		int countLine = 1;

		try (BufferedReader br = new BufferedReader(new FileReader(logBroker))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (countLine > 1) {
					linhas.add(mapeiaLinha(line.split(",")));
				}
				countLine++;
			}
		}

		return linhas;
	}

	private static Linha mapeiaLinha(String[] partes) {
		long time = Long.parseLong(partes[0]);
		String tag = partes[1];
		String idEvento = null;
		String idSubscricao = null;

		switch (tag) {
		case TOKEN_EVENTO_RECEBIDO:
		case TOKEN_EVENTO_PROCESSADO:
			idEvento = partes[2];
			break;
		case TOKEN_NOVA_SUBSCRICAO:
			idSubscricao = partes[2];
			break;
		case TOKEN_EVENTO_ENVIADO_SUBS:
			idEvento = partes[2];
			idSubscricao = partes[3];
			break;
		default:
			break;
		}
		return new Linha(time, tag, idEvento, idSubscricao);
	}

	private static void criarMapaSubscricao(List<Linha> linhas) throws IOException {

		clearTheFile();
		FileWriter fw = new FileWriter(resultado);
		BufferedWriter bw = new BufferedWriter(fw);

		Map<String, List<String>> mapa = new HashMap<>();

		for (Linha linha : linhas) {
			if (TOKEN_NOVA_SUBSCRICAO.equals(linha.getTag())) {
				mapa.put(linha.getIdSubscricao(), new ArrayList<>());
			}
		}

		for (Linha linha : linhas) {
			if (TOKEN_EVENTO_ENVIADO_SUBS.equals(linha.getTag())) {
				mapa.get(linha.getIdSubscricao()).add(linha.getIdEvento());
			}
		}

		for (Map.Entry<String, List<String>> entry : mapa.entrySet()) {

			String line = String.format("%s	%s", entry.getKey(), entry.getValue());
			bw.write(line);
			bw.newLine();
		}
		bw.flush();
		bw.close();
		fw.close();
	}

	public static void clearTheFile() throws IOException {
		FileWriter fwOb = new FileWriter(resultado, false);
		PrintWriter pwOb = new PrintWriter(fwOb, false);
		pwOb.flush();
		pwOb.close();
		fwOb.close();
	}
}
