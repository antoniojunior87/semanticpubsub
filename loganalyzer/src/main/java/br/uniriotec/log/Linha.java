package br.uniriotec.log;

public class Linha {

	private Long time;
	private String tag;
	private String idEvento;
	private String idSubscricao;

	public Linha(Long time, String tag, String idEvento, String idSubscricao) {
		super();
		this.time = time;
		this.tag = tag;
		this.idEvento = idEvento;
		this.idSubscricao = idSubscricao;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(String idEvento) {
		this.idEvento = idEvento;
	}

	public String getIdSubscricao() {
		return idSubscricao;
	}

	public void setIdSubscricao(String idSubscricao) {
		this.idSubscricao = idSubscricao;
	}

}