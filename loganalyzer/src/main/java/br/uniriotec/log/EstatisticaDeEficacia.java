package br.uniriotec.log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

public class EstatisticaDeEficacia {

	/**
	 * P = Precisão:
	 * <p>
	 * mede a proporção de eventos relevantes descobertos em relação ao total de
	 * eventos entregues aos assinantes.
	 * <p>
	 * P = TP/(TP+FP)
	 */

	/**
	 * C = Cobertura:
	 * <p>
	 * mede a proporção de eventos relevantes em relação ao total de eventos
	 * relevantes do gabarito.
	 * <p>
	 * C = TP/(TP+FN)
	 */

	/**
	 * F-Score:
	 * <p>
	 * Harmonia entre precisão e cobertura.
	 * <p>
	 * F = (2 x P x C)/(P + C)
	 */

	/**
	 * TP = True Positives:
	 * <p>
	 * Elementos presentes em selectedElements e que estão em relevantElements.
	 */

	/**
	 * FP = False Positives:
	 * <p>
	 * Elementos presentes em selectedElements e que NÃO estão em
	 * relevantElements.
	 */

	/**
	 * FN = False Negatives:
	 * <p>
	 * Elementos presentes em relevantElements que NÂO foram selecionados em
	 * selectedElements.
	 */
	private static final String relevantElements = "/tmp/relevance-map.tsv";
	private static final String selectedElements = "/tmp/resultado.tsv";

	public static ResultadoEficacia obterResultado() throws Exception {

		LinkedList<String> listaRelevantElements = obterListaItens(relevantElements);
		LinkedList<String> listaSelectedElements = obterListaItens(selectedElements);

		int[] trueFalsePositives = obterTruePositivesFalsePositives(listaRelevantElements, listaSelectedElements);

		int truePositives = trueFalsePositives[0];
		int falsePositives = trueFalsePositives[1];
		int falseNegatives = trueFalsePositives[2];

		BigDecimal precisao = precisao(truePositives, falsePositives);
		BigDecimal cobertura = cobertura(truePositives, falseNegatives);
		BigDecimal fscore = fScore(precisao, cobertura);

		ResultadoEficacia resultado = new ResultadoEficacia();
		resultado.setTruePositives(truePositives);
		resultado.setFalsePositives(falsePositives);
		resultado.setFalseNegatives(falseNegatives);
		resultado.setPrecisao(precisao);
		resultado.setCobertura(cobertura);
		resultado.setFscore(fscore);

		return resultado;
	}

	private static int[] obterTruePositivesFalsePositives(LinkedList<String> listaRelevantElements,
			LinkedList<String> listaSelectedElements) {

		int countTruePositives = 0;
		int countFalsePositives = 0;
		int countFalseNegatives = 0;

		for (String itemSelecionado : listaSelectedElements) {
			if (listaRelevantElements.contains(itemSelecionado)) {
				countTruePositives++;
			} else {
				countFalsePositives++;
			}
		}

		for (String itemRelevante : listaRelevantElements) {
			if (!listaSelectedElements.contains(itemRelevante)) {
				countFalseNegatives++;
			}
		}

		return new int[] { countTruePositives, countFalsePositives, countFalseNegatives };
	}

	private static LinkedList<String> obterListaItens(String arquivo) throws Exception {

		LinkedList<String> lista = new LinkedList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(arquivo))) {
			String line;
			while ((line = br.readLine()) != null) {

				String[] partes = line.split("\t");

				String idSubscricao = partes[0].trim();

				String[] valores = partes[1].replaceAll("\\[", "").replaceAll("\\]", "").split(", ");
				for (String idEvento : valores) {
					if (!"".equals(idEvento)) {
						lista.add(String.format("%s_%s", idSubscricao, idEvento));
					}
				}
			}
		}
		return lista;
	}

	public static BigDecimal precisao(int truePositives, int falsePositives) {

		BigDecimal bTruePositives = BigDecimal.valueOf(truePositives);
		BigDecimal bFalsePositives = BigDecimal.valueOf(falsePositives);
		BigDecimal soma = bTruePositives.add(bFalsePositives);

		return BigDecimal.ZERO.equals(soma) ? BigDecimal.ZERO : bTruePositives.divide(soma, 5, RoundingMode.HALF_UP);
	}

	public static BigDecimal cobertura(int truePositives, int falseNegatives) {

		BigDecimal bTruePositives = BigDecimal.valueOf(truePositives);
		BigDecimal bFalseNegatives = BigDecimal.valueOf(falseNegatives);
		BigDecimal soma = bTruePositives.add(bFalseNegatives);

		return bTruePositives.divide(soma, 5, RoundingMode.HALF_UP);
	}

	public static BigDecimal fScore(int truePositives, int falsePositives, int falseNegatives) {
		BigDecimal precisao = precisao(truePositives, falsePositives);
		BigDecimal cobertura = cobertura(truePositives, falseNegatives);
		return fScore(precisao, cobertura);
	}

	public static BigDecimal fScore(BigDecimal precisao, BigDecimal cobertura) {

		BigDecimal soma = precisao.add(cobertura);
		if (soma.compareTo(BigDecimal.ZERO) == 0) {
			return BigDecimal.ZERO;
		}
		return BigDecimal.valueOf(2).multiply(precisao).multiply(cobertura).divide(soma, 5, RoundingMode.HALF_UP);
	}

}
