package br.uniriotec.log;

public interface ILogEvento extends Comparable<ILogEvento> {

	public String getIdentificador();
}
