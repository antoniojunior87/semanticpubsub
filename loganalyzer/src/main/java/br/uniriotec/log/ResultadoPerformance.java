package br.uniriotec.log;

public class ResultadoPerformance {

	private Double tempoMedio = 0d;
	private Double taxaNotificacao = 0d;

	public Double getTempoMedio() {
		return tempoMedio;
	}

	public void setTempoMedio(Double tempoMedio) {
		this.tempoMedio = tempoMedio;
	}

	public Double getTaxaNotificacao() {
		return taxaNotificacao;
	}

	public void setTaxaNotificacao(Double taxaNotificacao) {
		this.taxaNotificacao = taxaNotificacao;
	}

}